var _bitacoraDashboard;

var _selectedPdf = []; 
var selectedElem;
var selectedOrden;
var _estadisticaData;
var _table;
var _tablePendientes;
var _listReportesPdf;
var _urlZipDescarga;
var _urlZipDescargaMasiva;
var _objReporteCovid;
var _base64imagen="";

$(document).ready(function() {
	_javascriptTemplate = new JavascriptTemplate();
	_bitacoraDashboard = new Dashboard();
	_bitacoraDashboard.init();
	
	
	
});

function mayus(e) {
    e.value = e.value.toUpperCase();
}


(function(a){a.createModal=function(b){defaults={title:"",message:"Your Message Goes Here!",closeButton:true,scrollable:false};var b=a.extend({},defaults,b);var c=(b.scrollable===true)?'style="max-height: 420px;overflow-y: auto;"':"";html='<div class="modal fade" id="myModal">';html+='<div class="modal-dialog">';html+='<div class="modal-content">';html+='<div class="modal-header">';html+='<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';if(b.title.length>0){html+='<h4 class="modal-title">'+b.title+"</h4>"}html+="</div>";html+='<div class="modal-body" '+c+">";html+=b.message;html+="</div>";html+='<div class="modal-footer">';if(b.closeButton===true){html+='<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>'}html+="</div>";html+="</div>";html+="</div>";html+="</div>";a("body").prepend(html);a("#myModal").modal().on("hidden.bs.modal",function(){a(this).remove()})}})(jQuery);

function Dashboard() {

	/**
	 * Inicializa los componentes necesarios para operar
	 */
	this.init = function() {
//		$('#msgDefaultSuccess').html('<h7>pruebas lñkañlsdk ñlkñlksdas ñlaskñdlak qwñlkñdlas qwñlkdkas ñlaskdñl wñlkasdñlk ñlasdñlak</h7></br>');
		
		console.log("entra init");
//		$('#modalSuccess').modal('show');
//		$('#mensajeModal').modal('show');
		
		_bitacoraDashboard.loadDataTable('');
		$('.select2-behavior').select2({
			theme : "bootstrap",
			width : "100%"
		});
				
		$( function() {
//			 $('#.date-class').datetimepicker();
		    $( ".date-class" ).datepicker({ dateFormat: 'yy-mm-dd' });
		} );
		
		
	}
	
	
	
	
	
	
	
	
	$('body')
	.on(
			'click',
			'#btnProcesarRecarga',
			function() {
				
				var idExpediente = $('#idExpediente').val();
				var monto = $('#selectMontoRecarga').val();
				var user = $('#idUser').val();
				
				var procesa = false;
				
				if(monto==-1){
					$('#selectMontoRecarga').addClass('is-invalid');
					procesa = false;
				}else{
					$('#selectMontoRecarga').removeClass('is-invalid');
					procesa = true;
				}
				
				if(procesa){
					$('#recargaModal').modal('hide');
					var ajax = new AjaxUtil();
					ajax.sendRequest('post', _contextPath + 'stempel-ventas/generar-recarga/'+idExpediente+'/'+monto+'/'+user, null,
							_bitacoraDashboard.successGenerarRecarga,
							_bitacoraDashboard.errorGenerarRecarga);	
				}
				
			});
	
	
	this.successGenerarRecarga = function(data){
		console.log(data);
		$('#msgDefaultSuccess').empty();
		$('#msgDefaultSuccess').html('<h7> '
				+ data + '</h7></br>');
		$('#modalSuccess').modal('show');
	}
	
	this.errorGenerarRecarga = function(data){
		console.log(data);
		alert(data);
	}
	
	
	
	
	
	
	
	
	
	$('body')
	.on(
			'click',
			'.check-generar-recarga',
			function() {
				
				$('#idExpediente').val($(this).attr('id'));
				$('#recargaModal').modal('show');
				
			});
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	$('body')
	.on(
			'click',
			'#buttonAgregarExp',
			function() {
				
				var expediente = $('#inputExpediente').val();
				var nombre = $('#inputNombre').val();
				var paterno = $('#inputApPaterno').val();
				var materno = $('#inputApMaterno').val();
				var user = $('#idUser').val();
				
				if(expediente == ''){
					expediente = 0;
				}
				
				filtro = new FiltroExpedienteDto(expediente, nombre, paterno, materno, user);
								
				var jsonFiltro = JSON.stringify(filtro);
				console.log(jsonFiltro);
				if(_bitacoraDashboard.validarFormExpediente(filtro)){
					
					var ajax = new AjaxUtil();
					ajax.sendRequest('post', _contextPath + 'stempel-ventas/crear-expediente', jsonFiltro,
							_bitacoraDashboard.successCrearExpediente,
							_bitacoraDashboard.errorCrearExpediente);	
				}
				
				
			});
	
	this.successCrearExpediente = function(data){
		$('#msgDefaultSuccess').empty();
		$('#modalSuccess').modal('show');
		
		$('#inputExpediente').val(data);
		$("#inputExpediente").prop( "disabled", false );
		
		_bitacoraDashboard.generarBusqueda();
		
//		if(data.estatus =='Error'){
//			$('#msgDefaultError').empty();
//			$('#msgDefaultError').html('</br></br><h6> Err: '
//							+ data.descripcion + '</h6></br>');
//			$('#modalError').modal('show');
//		}else{			
//			$('#msgDefaultSuccess').empty();
//			$('#modalSuccess').modal('show');
//		}
		
	}
	
	this.errorCrearExpediente = function(data){
		console.log(data);
//		$('#msgDefaultError2').empty();
//		$('#msgDefaultError2').html('</br></br><h7> Err: '
//						+ data.responseText + '</h7></br>');
//		$('#modalCrearPersona').modal('hide');
//		$('#modalError2').modal('show');
	}
	
	
	this.validarFormExpediente = function(object){
		var validacion= true;
		
		if(object.nombre==""){
			validacion = false;
			$('#inputNombre').addClass('is-invalid');
		}else{
			$('#inputNombre').removeClass('is-invalid');
		}
		if(object.paterno==""){
			validacion = false;
			$('#inputApPaterno').addClass('is-invalid');
		}else{
			$('#inputApPaterno').removeClass('is-invalid');
		}
		if(object.materno==""){
			validacion = false;
			$('#inputApMaterno').addClass('is-invalid');
		}else{
			$('#inputApMaterno').removeClass('is-invalid');
		}
		
		return validacion;
	}
	
	
	
	
	
	$('body')
	.on(
			'click',
			'#buttonBuscar',
			function() {
				_bitacoraDashboard.generarBusqueda();
				
			});
	
	this.generarBusqueda = function(){
		$("#inputExpediente").prop( "disabled", false );
		$('#idDivAgregar').addClass('d-none');
		var expediente = $('#inputExpediente').val();
		var nombre = $('#inputNombre').val();
		var paterno = $('#inputApPaterno').val();
		var materno = $('#inputApMaterno').val();
		var user = $('#inputNombre').val();
		
		if(expediente == ''){
			expediente = 0;
		}
		
		filtro = new FiltroExpedienteDto(expediente, nombre, paterno, materno, user);
						
		var jsonFiltro = JSON.stringify(filtro);
		console.log(jsonFiltro);
		
		var ajax = new AjaxUtil();
		ajax.sendRequest('post', _contextPath + 'stempel-ventas/get-expedientes', jsonFiltro,
				_bitacoraDashboard.successGetExpedientes,
				_bitacoraDashboard.errorGetExpedientes);
	}
	
	this.successGetExpedientes = function(data){
		console.log(data);
		if(data.length>0){
			_bitacoraDashboard.loadDataTable(data);
		}else{
			_bitacoraDashboard.loadDataTable(data);
			$('#mensajeModal').modal('show');
			$('#idDivAgregar').removeClass('d-none');
			$('#inputExpediente').val("");
			$("#inputExpediente").prop( "disabled", true );
		}
		
	}
	
	this.errorGetExpedientes = function(data){
		console.log(data);
	}
	
	
	
	
	
	
	
	
	
	
	
	this.loadDataTable = function(jsonPagos) {
		if (_table != null) {
			$("#bodyResultadosDetalle").empty();
			_table.destroy();
		}
		_table = $('#tableResultadosDetalle')
				.DataTable(
						{
							"data" : jsonPagos,
							"language" : {
								"url" : "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
							},
							"columns" : [
									{
										"data" : "expediente",
										"render" : function(data, type, full,
												meta) {
											var link = '';
//												link = '<input style="" class="form-check-input check-generar-recarga" type="checkbox"  id="'+data+'" >';
												link = '<btn id="'+data+'" class="btn btn-sm btn-outline-success btn-round btn-icon check-generar-recarga"><i class="fas fa-edit"></i> </btn>';
											
											return '<div class="text-center" style="font-weight: bold; ">'
													+ link + '</div>';
										}
									},
									{
										"data" : "expediente",
										"render" : function(data, type, full,
												meta) {
											return '<div class="text-center" style="font-weight: bold;">'
													+ data + '</div>';
										}
									},
									{
										"data" : "nombre",
										"render" : function(data, type, full,
												meta) {
											
											return '<div  class="text-center"  style="font-weight: bold;">'
													+ data + '</div>';
										}

									}
									
									 ],
							"order" : [ [ 0, "desc" ] ],
						});
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	this.crearObjectPaciente = function(){
		
		var convenio = $('#selectConvenio').val();
		var nombre = $('#inputNombreCrea').val();
		var paterno = $('#inputPaternoCrea').val();
		var materno = $('#inputMaternoCrea').val();
		var sexo = $('#selectSexoPaciente').val();
		var nacimiento = $('#selectFechaNacimiento').val();
		var correo = $('#inputCorreo').val();
		var telefono = $('#inputTelefono').val();
		var gruposanguineo = $('#inputSanguineo').val();
		var estadocivil =  $('#inputEstadoCivil').val();
		var religion = $('#inputReligion').val();
		var calle = $('#inputCalle').val();
		var colonia = $('#inputColonia').val();
		var ciudad = $('#inputCiudad').val();
		var estado = $('#inputEstado').val();
		var pais = $('#inputPais').val();
		var postal = $('#inputCodPostal').val();
		var base64imagen =_base64imagen;
		
		objectPaciente = new PacienteDto(null, nombre, paterno, materno, sexo, nacimiento,
				correo, telefono, calle, colonia, ciudad, estado, pais,
				postal, convenio, gruposanguineo, estadocivil, religion,
				base64imagen, 0, '');
		
		
		var diabetes = $('input:radio[name=optradioDiabetes]:checked').val();
		
		var patentescoMamaDiabetes = $('#idCBDiabetesMama').is(':checked');
		var patentescoPapaDiabetes = $('#idCBDiabetesPapa').is(':checked');
		var patentescoHnosDiabetes = $('#idCBDiabetesHno').is(':checked');
		var patentescoAbuelosDiabetes = $('#idCBDiabetesAbuelos').is(':checked');
		var patentescoOtrosDiabetes = $('#idCBDiabetesOtros').is(':checked');
		var vasculares = $('input:radio[name=optradioVascular]:checked' ).val(); 
		var patentescoMamaVasculares = $('#idCBVascularMama').is(':checked');
		var patentescoPapaVasculares = $('#idCBVascularPapa').is(':checked');
		var patentescoHnosVasculares = $('#idCBVascularHno').is(':checked');
		var patentescoAbuelosVasculares = $('#idCBVascularAbuelos').is(':checked');
		var patentescoOtrosVasculares = $('#idCBVascularOtros').is(':checked');
		var hipertension = $('input:radio[name=optradioHiperten]:checked' ).val();
		var patentescoMamaHipertension = $('#idCBHipertenMama').is(':checked');
		var patentescoPapaHipertension = $('#idCBHipertenPapa').is(':checked');
		var patentescoHnosHipertension = $('#idCBHipertenHno').is(':checked');
		var patentescoAbuelosHipertension = $('#idCBHipertenAbuelos').is(':checked');
		var patentescoOtrosHipertension = $('#idCBHipertenOtros').is(':checked');
		var renales = $('input:radio[name=optradioRenal]:checked' ).val();
		var patentescoMamaRenales = $('#idCBRenalMama').is(':checked');
		var patentescoPapaRenales = $('#idCBRenalPapa').is(':checked');
		var patentescoHnosRenales = $('#idCBRenalHno').is(':checked');
		var patentescoAbuelosRenales = $('#idCBRenalAbuelos').is(':checked');
		var patentescoOtrosRenales = $('#idCBRenalOtros').is(':checked');
		var reumatologico = $('input:radio[name=optradioReumatologo]:checked' ).val();
		var patentescoMamaReumatologico = $('#idCBReumatologoMama').is(':checked');
		var patentescoPapaReumatologico = $('#idCBReumatologoPapa').is(':checked');
		var patentescoHnosReumatologico = $('#idCBReumatologoHno').is(':checked');
		var patentescoAbuelosReumatologico = $('#idCBReumatologoAbuelos').is(':checked');
		var patentescoOtrosReumatologico = $('#idCBReumatologoOtros').is(':checked');
		var cancer = $('input:radio[name=optradioCancer]:checked' ).val();
		var patentescoMamaCancer = $('#idCBCancerMama').is(':checked');
		var patentescoPapaCancer = $('#idCBCancerPapa').is(':checked');
		var patentescoHnosCancer = $('#idCBCancerHno').is(':checked');
		var patentescoAbuelosCancer = $('#idCBCancerAbuelos').is(':checked');
		var patentescoOtrosCancer = $('#idCBCancerOtros').is(':checked');
		
		objectHeredo = new HeredofamiliaresDto(diabetes, patentescoMamaDiabetes, patentescoPapaDiabetes,
				patentescoHnosDiabetes, patentescoAbuelosDiabetes, patentescoOtrosDiabetes,
				vasculares, patentescoMamaVasculares, patentescoPapaVasculares,
				patentescoHnosVasculares, patentescoAbuelosVasculares, patentescoOtrosVasculares,
				hipertension, patentescoMamaHipertension, patentescoPapaHipertension,
				patentescoHnosHipertension, patentescoAbuelosHipertension,
				patentescoOtrosHipertension, renales, patentescoMamaRenales,
				patentescoPapaRenales, patentescoHnosRenales, patentescoAbuelosRenales,
				patentescoOtrosRenales, reumatologico, patentescoMamaReumatologico,
				patentescoPapaReumatologico, patentescoHnosReumatologico,
				patentescoAbuelosReumatologico, patentescoOtrosReumatologico, cancer,
				patentescoMamaCancer, patentescoPapaCancer, patentescoHnosCancer,
				patentescoAbuelosCancer, patentescoOtrosCancer);
		
		
		
		
		
		var frecuenciaBano = $('#idFrecBano').val();
		var frecuenciaDental = $('#idFrecAseoDental').val();
		var vacunacionCompleta = $('input:radio[name=optradioVacunacion]:checked').val();
		var practicaDeporte = $('input:radio[name=optPracDepor]:checked').val();
		var deporteFrecuencia = $('#idFrecuenDeporte').val();
		var fuma = $('input:radio[name=optFuma]:checked').val();
		var cigarrosPorSemana = $('#idCigarrosSemana').val();
		var dejoDeFumar = $('#idCuadoDejoFumar').val();
		var tomaAlcohol = $('input:radio[name=optTomaAlcohol]:checked').val();
		var edadInicio = $('#idEdadInicioAlcohol').val();
		var alcoholFrecuencia = $('#idFrecuenciaAlcohol').val();
		var usadoDrogasCual = $('#idCualDroga').val();
		var ultimaVezUsoDrogas = $('#idUltimaVezDroga').val();
		
		
		objectNoPatologico = new NoPatologicosDto(frecuenciaBano, frecuenciaDental, vacunacionCompleta,
				practicaDeporte, deporteFrecuencia, fuma, cigarrosPorSemana,
				dejoDeFumar, tomaAlcohol, edadInicio, alcoholFrecuencia,
				usadoDrogasCual, ultimaVezUsoDrogas);
		
		
		
		
		var asma = $('input:radio[name=optAsma]:checked').val();
		var bronquitis = $('input:radio[name=optBronquites]:checked').val();
		var neumonia = $('input:radio[name=optNeumonia]:checked').val();
		var tuberculosis = $('input:radio[name=optTuberculosis]:checked').val();
		var tosPersistente = $('input:radio[name=optTosPersistente]:checked').val();
		var faltaAire = $('input:radio[name=optFaltaAire]:checked').val();
		var gastritis = $('input:radio[name=optGastritis]:checked').val();
		var colitis = $('input:radio[name=optColitis]:checked').val();
		var hepatitisTipoA = $('input:radio[name=optHepatitisA]:checked').val();
		var ulceraPeptica = $('input:radio[name=optUlceraPeptica]:checked').val();
		var lumbalgia = $('input:radio[name=optLumbalgia]:checked').val();
		var fracturas = $('input:radio[name=optFracturas]:checked').val();
		var esguinces = $('input:radio[name=optEsguinces]:checked').val();
		var palpitaciones = $('input:radio[name=optPalpitaciones]:checked').val();
		var fiebreReumatica = $('input:radio[name=optFiebreReum]:checked').val();
		var hipertesion = $('input:radio[name=optHipertension]:checked').val();
		var varices = $('input:radio[name=optVarices]:checked').val();
		var traumaticos = $('input:radio[name=optAnteTraumaticos]:checked').val();
		var desmayo = $('input:radio[name=optDesmayo]:checked').val();
		var convulsiones = $('input:radio[name=optConvulsiones]:checked').val();
		var migraña = $('input:radio[name=optMigrana]:checked').val();
		var vertigo = $('input:radio[name=optVertigo]:checked').val();
		var manchas = $('input:radio[name=optManchas]:checked').val();
		var onicomicosis = $('input:radio[name=optOnicomicosis]:checked').val();
		var cantidadTatuajes = $('#idCantTatuaje').val();
		var tatuajes = $('input:radio[name=optTatuajes]:checked').val();
		var cantidadPerforaciones = $('#idCantPerforacion').val();
		var perforaciones = $('input:radio[name=optPerforaciones]:checked').val();
		var diabetes = $('input:radio[name=optDiabetes]:checked').val();
		var hipertiroidismo = $('input:radio[name=optHipertiro]:checked').val();
		var hipotiroidismo = $('input:radio[name=optHipotiroidismo]:checked').val();
		var cirugias = $('#idTxtCirugias').val();
		var transfunciones = $('#idTransfuciones').val();
		var padecimientoActual = $('#idTxtPadecimientoActual').val();
		var fobias = $('#idTxtFobias').val();
		var alergias = $('#idTxtAlergias').val();
		
		
		objectPatologico = new PatologicosDto(asma, bronquitis, neumonia, tuberculosis,
				tosPersistente, faltaAire, gastritis, colitis, hepatitisTipoA,
				ulceraPeptica, lumbalgia, fracturas, esguinces, palpitaciones,
				fiebreReumatica, hipertesion, varices, traumaticos, desmayo,
				convulsiones, migraña, vertigo, manchas, onicomicosis,
				cantidadTatuajes, tatuajes, cantidadPerforaciones, perforaciones,
				diabetes, hipertiroidismo, hipotiroidismo, cirugias, transfunciones,
				padecimientoActual, fobias, alergias);
		
		
		
		var empleosPrevios = $('#idEmpleosPrevios').val();
		var actividadesArea = $('#idActividadesArea').val();
		var antiguedad = $('#IdAntiguedad').val();
		var jornadaLaboralSemanal = $('#idJornadaLabSemana').val();
		var exposicionLaboral = $('#idExposicionLaboral').val();
		var tratamientosRecibidos = $('#idTratamientosRecibidos').val();
		var otrosTrabajos = $('#idOtrosTrabajos').val();
		
		
		objectLaborales = new LaboralesDto(empleosPrevios, actividadesArea, antiguedad, jornadaLaboralSemanal,
				exposicionLaboral, tratamientosRecibidos, otrosTrabajos);
		
		
		
		var pesoReal = $('#idPesoReal').val();
		var talla = $('#idTalla').val();
		var imc = $('#idImc').val();
		var frecuenciaCardiaca = $('#idFrecuenciaCardiaca').val();
		var frecuenciaRespiratoria = $('#idFrecuenciaRespiratoria').val();
		var temperatura = $('#idTemp').val();
		var ta = $('#idTA').val();
		var craneo = $('#idCraneo').val();
		var cabello = $('#idCabello').val();
		
		objectExploracionFisica = new ExploracionFisicaDto(pesoReal, talla, imc, frecuenciaCardiaca,
				frecuenciaRespiratoria, temperatura, ta, craneo, cabello);
		
		
		
		var labiosBoca = $('#idLabiosBoca').val();
		var organosDentales = $('#idOrganosDentales').val();
		var lenguaFaringe = $('#idLenguaFaringe').val();
		var amigdalas = $('#idAmigdalas').val();
		var septumaNasal = $('#idSeptumaNasal').val();
		var cornetes = $('#idCornetes').val();
		var oidos = $('#idOidos').val();
		var irisPupilas = $('#idIrisPupila').val();
		var conjuntivasEsclera = $('#idConjuntivasEsclera').val();
		var lineaVisual = $('#idLineaVisual').val();
		var utilizaLentes = $('input:radio[name=optLentes]:checked').val();
		var agudezaVisual = $('#idAgudezaVisual').val();
		var agudezaVisual2 = $('#idAgudezaVisual2').val();
		var percepcionCromatica = $('#idPercepcionCromatica').val();
		var visionCercana = $('#idVisionCercana').val();
		
		objectCara = new CaraDto(labiosBoca, organosDentales, lenguaFaringe, amigdalas,
				septumaNasal, cornetes, oidos, irisPupilas, conjuntivasEsclera,
				lineaVisual, utilizaLentes, agudezaVisual, agudezaVisual2,
				percepcionCromatica, visionCercana);
		
		
		
		
		
		
		var cuello = $('#idCuello').val();
		var areaPulmonar = $('#idAreaPulmonar').val();
		var areaCardiaca = $('#idAreaCardiaca').val();
		var abdomen = $('#idAbdomen').val();
		var miembrosToracicos = $('#idMiembrosToracicos').val();
		var miembrosPelvicos = $('#idMiembrosPelvicos').val();
		var columnaVertebral = $('#idColumnaVertebral').val();
		var genitalesExteriores = $('#idGenitalesExteriores').val();
		
		
		objectRegionesSegmentos = new RegionesSegmentosDto(cuello, areaPulmonar, areaCardiaca, abdomen,
				miembrosToracicos, miembrosPelvicos, columnaVertebral, genitalesExteriores);
		
		
		objectExpedientePaciente = new ExpedientePacienteDto(objectPaciente, objectHeredo,
				objectNoPatologico, objectPatologico, objectLaborales,
				objectExploracionFisica, objectCara, objectRegionesSegmentos);
		
		return objectExpedientePaciente;
		
	}
	
	
	
	
	
	$('body').on(
			'change',
			'#selectConvenio',
			function(){
				var convenio = $('#selectConvenio').val();

				if(convenio == 4889){
					$("#idThEdit1").html("Estudio Cl&iacute;nico");
					$("#idThText1").html("Fecha Estudio Cl&iacute;nico");
					$("#idThEdit2").html("Toxicol&oacute;gico");
					$("#idThText2").html("Fecha Toxicol&oacute;gico");
					$("#idThEdit3").html("Confirmatorio");
					$("#idThText3").html("Fecha Confirmatorio");					
				}else if(convenio == 4860){
					$("#idThEdit1").html("Confirmatorio");
					$("#idThText1").html("Fecha Confirmatorio");
					
					$("#idThEdit2").html("Estudio Cl&iacute;nico");
					$("#idThText2").html("Fecha Estudio Cl&iacute;nico");
					
					$("#idThEdit3").html(" ");
					$("#idThText3").html(" ");
				}else if(convenio == 4881){
					$("#idThEdit1").html("Toxicol&oacute;gico");
					$("#idThText1").html("Fecha Toxicol&oacute;gico");
					
					$("#idThEdit2").html("Estudio Cl&iacute;nico");
					$("#idThText2").html("Fecha Estudio Cl&iacute;nico");
					
					$("#idThEdit3").html(" ");
					$("#idThText3").html(" ");
				}
				
			}
		);
	
	
	
	
	$('body')
	.on(
			'click',
			'.btn-get-info',
			function() {
				
				var consecutivo = $(this).attr('idConsecutivo');
				var ajax = new AjaxUtil();
				ajax.sendRequest2('get',_contextPath + 'dashboard-eventos/get-paciente-by-consecutivo/'+consecutivo, null,
						_bitacoraDashboard.successGetPaciente,
						_bitacoraDashboard.errorGetPaciente);	
			});
	
	this.successGetPaciente = function(data){
		console.log(data);
//		var jsonObject = JSON.stringify(data);
//		console.log(jsonObject);
		$('#inputNombreCrea').val(data.pacienteDto.nombre);
		$('#inputPaternoCrea').val(data.pacienteDto.paterno);
		$('#inputMaternoCrea').val(data.pacienteDto.materno);
		$('#selectSexoPaciente').val(data.pacienteDto.sexo);
		$('#selectFechaNacimiento').val(data.pacienteDto.nacimiento);
		$('#inputCorreo').val(data.pacienteDto.correo);
		$('#inputTelefono').val(data.pacienteDto.telefono);
//		var splitDireccion = data.sdireccion.split(",");
		
		$('#inputCalle').val(data.pacienteDto.calle);
		$('#inputColonia').val(data.pacienteDto.colonia);
		$('#inputCiudad').val(data.pacienteDto.ciudad);
		$('#inputEstado').val(data.pacienteDto.estado);
		$('#inputPais').val(data.pacienteDto.pais);
		$('#inputCodPostal').val(data.pacienteDto.postal);
		$('#inputSanguineo').val(data.pacienteDto.gruposanguineo);
		$('#inputEstadoCivil').val(data.pacienteDto.estadocivil);
		$('#inputReligion').val(data.pacienteDto.religion);
		
		$('#idImgUser').attr({
			   'src': data.pacienteDto.base64imagen
			})
			
			
			
		$("input[name=optradioDiabetes][value="+data.heredofamiliaresDto.diabetes+"]").prop("checked",true);
		$("#idCBDiabetesMama").prop("checked", data.heredofamiliaresDto.patentescoMamaDiabetes);
		$("#patentescoPapaDiabetes").prop("checked", data.heredofamiliaresDto.patentescoPapaDiabetes);
		$("#idCBDiabetesHno").prop("checked", data.heredofamiliaresDto.patentescoHnosDiabetes);
		$("#idCBDiabetesAbuelos").prop("checked", data.heredofamiliaresDto.patentescoAbuelosDiabetes);
		$("#idCBDiabetesOtros").prop("checked", data.heredofamiliaresDto.patentescoOtrosDiabetes);

		$("input[name=optradioVascular][value="+data.heredofamiliaresDto.vasculares+"]").prop("checked",true);
		$("#idCBVascularMama").prop("checked", data.heredofamiliaresDto.patentescoMamaVasculares);
		$("#idCBVascularPapa").prop("checked", data.heredofamiliaresDto.patentescoPapaVasculares);
		$("#idCBVascularHno").prop("checked", data.heredofamiliaresDto.patentescoHnosVasculares);
		$("#idCBVascularAbuelos").prop("checked", data.heredofamiliaresDto.patentescoAbuelosVasculares);
		$("#idCBVascularOtros").prop("checked", data.heredofamiliaresDto.patentescoOtrosVasculares);
		
		$("input[name=optradioHiperten][value="+data.heredofamiliaresDto.hipertension+"]").prop("checked",true);
		$("#idCBHipertenMama").prop("checked", data.heredofamiliaresDto.patentescoMamaHipertension);
		$("#idCBHipertenPapa").prop("checked", data.heredofamiliaresDto.patentescoPapaHipertension);
		$("#idCBHipertenHno").prop("checked", data.heredofamiliaresDto.patentescoHnosHipertension);
		$("#idCBHipertenAbuelos").prop("checked", data.heredofamiliaresDto.patentescoAbuelosHipertension);
		$("#idCBHipertenOtros").prop("checked", data.heredofamiliaresDto.patentescoOtrosHipertension);
		
		$("input[name=optradioRenal][value="+data.heredofamiliaresDto.renales+"]").prop("checked",true);
		$("#idCBRenalMama").prop("checked", data.heredofamiliaresDto.patentescoMamaRenales);
		$("#idCBRenalPapa").prop("checked", data.heredofamiliaresDto.patentescoPapaRenales);
		$("#idCBRenalHno").prop("checked", data.heredofamiliaresDto.patentescoHnosRenales);
		$("#idCBRenalAbuelos").prop("checked", data.heredofamiliaresDto.patentescoAbuelosRenales);
		$("#idCBRenalOtros").prop("checked", data.heredofamiliaresDto.patentescoOtrosRenales);

		$("input[name=optradioReumatologo][value="+data.heredofamiliaresDto.reumatologico+"]").prop("checked",true);
		$("#idCBReumatologoMama").prop("checked", data.heredofamiliaresDto.patentescoMamaReumatologico);
		$("#idCBReumatologoPapa").prop("checked", data.heredofamiliaresDto.patentescoPapaReumatologico);
		$("#idCBReumatologoHno").prop("checked", data.heredofamiliaresDto.patentescoHnosReumatologico);
		$("#idCBReumatologoAbuelos").prop("checked", data.heredofamiliaresDto.patentescoAbuelosReumatologico);
		$("#idCBReumatologoOtros").prop("checked", data.heredofamiliaresDto.patentescoOtrosReumatologico);

		$("input[name=optradioCancer][value="+data.heredofamiliaresDto.cancer+"]").prop("checked",true);
		$("#idCBCancerMama").prop("checked", data.heredofamiliaresDto.patentescoMamaCancer);
		$("#idCBCancerPapa").prop("checked", data.heredofamiliaresDto.patentescoPapaCancer);
		$("#idCBCancerHno").prop("checked", data.heredofamiliaresDto.patentescoHnosCancer);
		$("#idCBCancerAbuelos").prop("checked", data.heredofamiliaresDto.patentescoAbuelosCancer);
		$("#idCBCancerOtros").prop("checked", data.heredofamiliaresDto.patentescoOtrosCancer);

		
		

	

		$('#idFrecBano').val(data.noPatologicosDto.frecuenciaBano);
		$('#idFrecAseoDental').val(data.noPatologicosDto.frecuenciaDental);
		$("input[name=optradioVacunacion][value="+data.noPatologicosDto.vacunacionCompleta+"]").prop("checked",true);
		$("input[name=optPracDepor][value="+data.noPatologicosDto.practicaDeporte+"]").prop("checked",true);
		$('#idFrecuenDeporte').val(data.noPatologicosDto.deporteFrecuencia);
		$("input[name=optFuma][value="+data.noPatologicosDto.fuma+"]").prop("checked",true);
		$('#idCigarrosSemana').val(data.noPatologicosDto.cigarrosPorSemana);
		$('#idCuadoDejoFumar').val(data.noPatologicosDto.dejoDeFumar);
		$("input[name=optTomaAlcohol][value="+data.noPatologicosDto.tomaAlcohol+"]").prop("checked",true);
		$('#idEdadInicioAlcohol').val(data.noPatologicosDto.edadInicio);
		$('#idFrecuenciaAlcohol').val(data.noPatologicosDto.alcoholFrecuencia);
		$('#idCualDroga').val(data.noPatologicosDto.usadoDrogasCual);
		$('#idUltimaVezDroga').val(data.noPatologicosDto.ultimaVezUsoDrogas);

		
		
		
		$("input[name=optAsma][value="+data.patologicosDto.asma+"]").prop("checked",true);
		$("input[name=optBronquites][value="+data.patologicosDto.bronquitis+"]").prop("checked",true);
		$("input[name=optNeumonia][value="+data.patologicosDto.neumonia+"]").prop("checked",true);
		$("input[name=optTuberculosis][value="+data.patologicosDto.tuberculosis+"]").prop("checked",true);
		$("input[name=optTosPersistente][value="+data.patologicosDto.tosPersistente+"]").prop("checked",true);
		$("input[name=optFaltaAire][value="+data.patologicosDto.faltaAire+"]").prop("checked",true);
		$("input[name=optGastritis][value="+data.patologicosDto.gastritis+"]").prop("checked",true);
		$("input[name=optColitis][value="+data.patologicosDto.colitis+"]").prop("checked",true);
		$("input[name=optHepatitisA][value="+data.patologicosDto.hepatitisTipoA+"]").prop("checked",true);
		$("input[name=optUlceraPeptica][value="+data.patologicosDto.ulceraPeptica+"]").prop("checked",true);
		$("input[name=optLumbalgia][value="+data.patologicosDto.lumbalgia+"]").prop("checked",true);
		$("input[name=optFracturas][value="+data.patologicosDto.fracturas+"]").prop("checked",true);
		$("input[name=optEsguinces][value="+data.patologicosDto.esguinces+"]").prop("checked",true);
		$("input[name=optPalpitaciones][value="+data.patologicosDto.palpitaciones+"]").prop("checked",true);
		$("input[name=optFiebreReum][value="+data.patologicosDto.fiebreReumatica+"]").prop("checked",true);
		$("input[name=optHipertension][value="+data.patologicosDto.hipertesion+"]").prop("checked",true);
		$("input[name=optVarices][value="+data.patologicosDto.varices+"]").prop("checked",true);
		$("input[name=optAnteTraumaticos][value="+data.patologicosDto.traumaticos+"]").prop("checked",true);
		$("input[name=optDesmayo][value="+data.patologicosDto.desmayo+"]").prop("checked",true);
		$("input[name=optConvulsiones][value="+data.patologicosDto.convulsiones+"]").prop("checked",true);
		$("input[name=optMigrana][value="+data.patologicosDto.migraña+"]").prop("checked",true);
		$("input[name=optVertigo][value="+data.patologicosDto.vertigo+"]").prop("checked",true);
		$("input[name=optManchas][value="+data.patologicosDto.manchas+"]").prop("checked",true);
		$("input[name=optOnicomicosis][value="+data.patologicosDto.onicomicosis+"]").prop("checked",true);
		$('#idCantTatuaje').val(data.patologicosDto.cantidadTatuajes);
		$("input[name=optTatuajes][value="+data.patologicosDto.tatuajes+"]").prop("checked",true);
		$('#idCantPerforacion').val(data.patologicosDto.cantidadPerforaciones);
		$("input[name=optPerforaciones][value="+data.patologicosDto.perforaciones+"]").prop("checked",true);
		$("input[name=optDiabetes][value="+data.patologicosDto.diabetes+"]").prop("checked",true);
		$("input[name=optHipertiro][value="+data.patologicosDto.hipertiroidismo+"]").prop("checked",true);
		$("input[name=optHipotiroidismo][value="+data.patologicosDto.hipotiroidismo+"]").prop("checked",true);
		$('#idTxtCirugias').val(data.patologicosDto.cirugias);
		$('#idTransfuciones').val(data.patologicosDto.transfunciones);
		$('#idTxtPadecimientoActual').val(data.patologicosDto.padecimientoActual);
		$('#idTxtFobias').val(data.patologicosDto.fobias);
		$('#idTxtAlergias').val(data.patologicosDto.alergias);


		
		$('#idEmpleosPrevios').val(data.laboralesDto.empleosPrevios);
		$('#idActividadesArea').val(data.laboralesDto.actividadesArea);
		$('#IdAntiguedad').val(data.laboralesDto.antiguedad);
		$('#idJornadaLabSemana').val(data.laboralesDto.jornadaLaboralSemanal);
		$('#idExposicionLaboral').val(data.laboralesDto.exposicionLaboral);
		$('#idTratamientosRecibidos').val(data.laboralesDto.tratamientosRecibidos);
		$('#idOtrosTrabajos').val(data.laboralesDto.otrosTrabajos);



		$('#idPesoReal').val(data.exploracionFisicaDto.pesoReal);
		$('#idTalla').val(data.exploracionFisicaDto.talla);
		$('#idImc').val(data.exploracionFisicaDto.imc);
		$('#idFrecuenciaCardiaca').val(data.exploracionFisicaDto.frecuenciaCardiaca);
		$('#idFrecuenciaRespiratoria').val(data.exploracionFisicaDto.frecuenciaRespiratoria);
		$('#idTemp').val(data.exploracionFisicaDto.temperatura);
		$('#idTA').val(data.exploracionFisicaDto.ta);
		$('#idCraneo').val(data.exploracionFisicaDto.craneo);
		$('#idCabello').val(data.exploracionFisicaDto.cabello);


		

		$('#idLabiosBoca').val(data.caraDto.labiosBoca);
		$('#idOrganosDentales').val(data.caraDto.organosDentales);
		$('#idLenguaFaringe').val(data.caraDto.lenguaFaringe);
		$('#idAmigdalas').val(data.caraDto.amigdalas);
		$('#idSeptumaNasal').val(data.caraDto.septumaNasal);
		$('#idCornetes').val(data.caraDto.cornetes);
		$('#idOidos').val(data.caraDto.oidos);
		$('#idIrisPupila').val(data.caraDto.irisPupilas);
		$('#idConjuntivasEsclera').val(data.caraDto.conjuntivasEsclera);
		$('#idLineaVisual').val(data.caraDto.lineaVisual);
		$("input[name=optLentes][value="+data.caraDto.utilizaLentes+"]").prop("checked",true);
		$('#idAgudezaVisual').val(data.caraDto.agudezaVisual);
		$('#idAgudezaVisual2').val(data.caraDto.agudezaVisual2);
		$('#idPercepcionCromatica').val(data.caraDto.percepcionCromatica);
		$('#idVisionCercana').val(data.caraDto.visionCercana);


		
		$('#idCuello').val(data.regionesSegmentosDto.cuello);
		$('#idAreaPulmonar').val(data.regionesSegmentosDto.areaPulmonar);
		$('#idAreaCardiaca').val(data.regionesSegmentosDto.areaCardiaca);
		$('#idAbdomen').val(data.regionesSegmentosDto.abdomen);
		$('#idMiembrosToracicos').val(data.regionesSegmentosDto.miembrosToracicos);
		$('#idMiembrosPelvicos').val(data.regionesSegmentosDto.miembrosPelvicos);
		$('#idColumnaVertebral').val(data.regionesSegmentosDto.columnaVertebral);
		$('#idGenitalesExteriores').val(data.regionesSegmentosDto.genitalesExteriores);

		
		
		$('#idLiInicio').removeClass('active');
		$('#idLiPerfil').addClass('active');
		$('#idFormBusqueda').addClass('d-none');
		$('#idFormPaciente').removeClass('d-none');
	}
	
	
	this.errorGetPaciente = function(data){
		console.log(data);
		$('#msgDefaultError2').empty();
		$('#msgDefaultError2').html('</br></br><h7> Err: '
						+ data.responseText + '</h7></br>');
		$('#modalCrearPersona').modal('hide');
		$('#modalError2').modal('show');
	}
	
	
	
	
	
	
	

	  
	/*
	 * Evento: Click 
	 */
	$('body')
			.on(
					'click',
					'.buttonTomarFotoAlumno',
					function() {
						$('#modalLog').modal('show');
						startVideo(1);
	});
	
	
	
	$('body')
	.on(
			'click',
			'#btnGuardar',
			function() {
				var canvas = document.getElementById('snapshot');
				var dataURL = canvas.toDataURL('image/jpeg', 1.0);
				
				_base64imagen = dataURL;
				
				$('#idImgUser').attr({
					   'src': dataURL
					})
					$('#modalLog').modal('hide');
	});
	
	
	
	
	
	
	
	
	
	
	
	  $('.solo-numero').keyup(function (){
	        this.value = (this.value + '').replace(/[^0-9]/g, '');
	      });
	  
	  $('.input-number').on('input', function () { 
		    this.value = this.value.replace(/[^0-9||,]/g,'');
		});
	
	  
	  
	  this.errorInit = function(data) {
			console.log(data);
		}
	  
	  
	  
	  
	  $('body')
		.on(
				'click',
				'.crear-paciente',
				function() {
					console.log("entraaaaa");
					$('#idLiInicio').removeClass('active');
					$('#idLiPerfil').addClass('active');
					$('#idFormBusqueda').addClass('d-none');
					$('#idFormPaciente').removeClass('d-none');
		});		 
	  
	  
	  
	  $('body')
		.on(
				'click',
				'.link-dashboard',
				function() {
					console.log("entraaaaa");
					$('#idLiInicio').addClass('active');
					$('#idLiPerfil').removeClass('active');
					$('#idFormBusqueda').removeClass('d-none');
					$('#idFormPaciente').addClass('d-none');
		});	
	  
	  
	  
	  
	  
	  
	  
	  
	  $('body')
		.on(
				'click',
				'#buttonCrearPaciente',
				function() {
					$('#idLiInicio').removeClass('active');
					$('#idLiPerfil').addClass('active');
					$('#idFormBusqueda').addClass('d-none');
					$('#idFormPaciente').removeClass('d-none');
//					console.log("entra");
//					$('#inputErrorNombreCrea').addClass('d-none');
//					$('#inputErrorPaternoCrea').addClass('d-none');
//					$('#inputErrorMaternoCrea').addClass('d-none');
//					$('#inputErrorFechaNacimiento').addClass('d-none');
//					$('#inputErrorCorreo').addClass('d-none');
//					$('#inputErrorTelefono').addClass('d-none');
//					$('#inputErrorCalle').addClass('d-none');
//					$('#inputErrorColonia').addClass('d-none');
//					$('#inputErrorCiudad').addClass('d-none');
//					$('#inputErrorEstado').addClass('d-none');
//					$('#inputErrorEstado').addClass('d-none');
//					$('#inputErrorPais').addClass('d-none');
//					$('#inputErrorCodPostal').addClass('d-none');
//					
//					$('#inputNombreCrea').val('');
//					$('#inputPaternoCrea').val("");
//					$('#inputMaternoCrea').val("");
//					$('#selectSexoPaciente').val(1);
//					$('#selectFechaNacimiento').val("");
//					$('#inputCorreo').val("");
//					$('#inputTelefono').val("");
//					$('#inputCalle').val("");
//					$('#inputColonia').val("");
//					$('#inputCiudad').val("");
//					$('#inputEstado').val("");
//					$('#inputPais').val("");
//					$('#inputCodPostal').val("");
//					$('#modalCrearPersona').modal('show');
					
					
		});		  
	  
	  
	  
	  
	  
	  $('body')
		.on(
				'click',
				'#buttonModalCrearPaciente',
				function() {
					
					objectExpediente = _bitacoraDashboard.crearObjectPaciente();
					
//					var convenio = $('#selectConvenio').val();
//					
//					var nombre = $('#inputNombreCrea').val();
//					var paterno = $('#inputPaternoCrea').val();
//					var materno = $('#inputMaternoCrea').val();
//					var sexo = $('#selectSexoPaciente').val();
//					var nacimiento = $('#selectFechaNacimiento').val();
//					var correo = $('#inputCorreo').val();
//					var telefono = $('#inputTelefono').val();
//					var calle = $('#inputCalle').val();
//					var colonia = $('#inputColonia').val();
//					var ciudad = $('#inputCiudad').val();
//					var estado = $('#inputEstado').val();
//					var pais = $('#inputPais').val();
//					var postal = $('#inputCodPostal').val();
//
//					filtro = new CrearPacienteDto(nombre, paterno, materno, sexo, nacimiento,
//							correo, telefono, calle, colonia, ciudad, estado, pais,
//							postal,convenio);
					
					
					var jsonFiltro = JSON.stringify(objectExpediente);
					console.log(jsonFiltro);
					if(_bitacoraDashboard.validarCamposCrearPaciente(objectExpediente)){
						var ajax = new AjaxUtil();
						ajax.sendRequest2('post',_contextPath + 'dashboard-eventos/crear-paciente', jsonFiltro,
								_bitacoraDashboard.successCrearPaciente,
								_bitacoraDashboard.errorCrearPaciente);						
					}
		});		  
	  
	  this.validarCamposCrearPaciente = function(objectExpediente){
		  object = objectExpediente.pacienteDto;
			var validacion= true;
			
			if(object.nombre==""){
				validacion = false;
				$('#inputErrorNombreCrea').removeClass('d-none');
			}else{
				$('#inputErrorNombreCrea').addClass('d-none');
			}
			if(object.paterno == ""){
				validacion = false;
				$('#inputErrorPaternoCrea').removeClass('d-none');
			}else{
				$('#inputErrorPaternoCrea').addClass('d-none');
			}
			if (object.materno == ""){
				validacion = false;
				$('#inputErrorMaternoCrea').removeClass('d-none');
			}else{
				$('#inputErrorMaternoCrea').addClass('d-none');
			}
			if (object.nacimiento == ""){
				validacion = false;
				$('#inputErrorFechaNacimiento').removeClass('d-none');
			}else{
				$('#inputErrorFechaNacimiento').addClass('d-none');
			}
			if (object.correo == ""){
				validacion = false;
				$('#inputErrorCorreo').removeClass('d-none');
			}else{
				$('#inputErrorCorreo').addClass('d-none');
			}
			if (object.telefono == ""){
				validacion = false;
				$('#inputErrorTelefono').removeClass('d-none');
			}else{
				$('#inputErrorTelefono').addClass('d-none');
			}
			if (object.calle == ""){
				validacion = false;
				$('#inputErrorCalle').removeClass('d-none');
			}else{
				$('#inputErrorCalle').addClass('d-none');
			}
			if (object.colonia == ""){
				validacion = false;
				$('#inputErrorColonia').removeClass('d-none');
			}else{
				$('#inputErrorColonia').addClass('d-none');
			}
			if (object.ciudad == ""){
				validacion = false;
				$('#inputErrorCiudad').removeClass('d-none');
			}else{
				$('#inputErrorCiudad').addClass('d-none');
			}
			if (object.estado == ""){
				validacion = false;
				$('#inputErrorEstado').removeClass('d-none');
			}else{
				$('#inputErrorEstado').addClass('d-none');
			}
			if (object.estado == ""){
				validacion = false;
				$('#inputErrorEstado').removeClass('d-none');
			}else{
				$('#inputErrorEstado').addClass('d-none');
			}
			if (object.pais == ""){
				validacion = false;
				$('#inputErrorPais').removeClass('d-none');
			}else{
				$('#inputErrorPais').addClass('d-none');
			}
			if (object.postal == ""){
				validacion = false;
				$('#inputErrorCodPostal').removeClass('d-none');
			}else{
				$('#inputErrorCodPostal').addClass('d-none');
			}
			
			return validacion;		
		}
	  
	  this.successCrearPaciente = function(data) {
			console.log(data);
			
			
			$('#idFormPaciente').closest('#idFormPaciente').find("input").each(function(i, v) {
	            $(this).val("");
	        });
			$('#idFormPaciente').closest('#idFormPaciente').find("textarea").each(function(i, v) {
	            $(this).val("");
	        });
						
			
			$('#idFormPaciente').closest('#idFormPaciente').find('.radio-btn').each(function(i, v) {
				$(this).prop('checked', false);
	        });
			
			$('#idFormPaciente').closest('#idFormPaciente').find('.radio-btn-no').each(function(i, v) {
				$(this).prop('checked', true);
	        });
			
			$('#idCantTatuaje').val('0');
			$('#idCantPerforacion').val('0');
			
			
			$('#modalCrearPersona').modal('hide');
			$('#msgDefaultSuccess').empty();
			$('#msgDefaultSuccess').html('<h7> Folio Generado: '
					+ data.orden.sordenexterna + '</h7></br>');
			$('#modalSuccess').modal('show');
			
			$('#idLiInicio').addClass('active');
			$('#idLiPerfil').removeClass('active');
			$('#idFormBusqueda').removeClass('d-none');
			$('#idFormPaciente').addClass('d-none');
			
			
			var convenio = [];
			filtro = new FilterCovidDto(0, 0, convenio, '', '',
					0, "", "", "", "", 0,"","","","",0,0,
					0, 0, 0, "", true, data.orden.sordenexterna, data.orden.cconvenio);
			
			
			var jsonFiltro = JSON.stringify(filtro);
			console.log(jsonFiltro);
			
			
			var ajax = new AjaxUtil();
			ajax.sendRequest('post', _contextPath + 'dashboard-eventos/get-reporte-evento', jsonFiltro,
					_bitacoraDashboard.successEstadisticaErrores,
					_bitacoraDashboard.errorReporteCovid);	
				
			
		}
	  
	  this.errorCrearPaciente = function(data) {
			console.log(data);
			$('#msgDefaultError2').empty();
			$('#msgDefaultError2').html('</br></br><h7> Err: '
							+ data.responseText + '</h7></br>');
			$('#modalCrearPersona').modal('hide');
			$('#modalError2').modal('show');
		}
	  
	  
	  $('body').on('click', '#btnDefaultError2', function() {
			$('#modalError2').modal('hide');
			$('#modalCrearPersona').modal('show');
		});
	  
	  
	  
	 
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  $('body').on(
				'change',
				'#selectLiberada',
				function(){
					
					if($('#selectLiberada').val() == 1 || $('#selectLiberada').val() == -1){
						$('#selectFechaLiberacion').prop('disabled', false);
						$('#selectFechaLiberacion').val("");
						$('#selectHoraLiberacion').prop('disabled', false);
						$('#selectHoraLiberacion').val("");
//						$('#selectOperacion').prop('disabled', false);
//						$('#selectOperacion').val("=");						
					}else{
						$('#selectFechaLiberacion').prop('disabled', true);
						$('#selectFechaLiberacion').val("");
						$('#selectHoraLiberacion').prop('disabled', true);
						$('#selectHoraLiberacion').val("");
//						$('#selectOperacion').prop('disabled', true);
//						$('#selectOperacion').val("=");
					}
					
				}
			);
	  
	  
	  
	  
	  
	  
	  
	  
	  $('body').on(
				'change',
				'#checkGrupal',
				function(){
					
					if($(this).prop('checked')){
						$('#divExamenes').addClass("d-none");
						$('#divExamenesGrupal').removeClass("d-none");
						$('#selectExamenGrupal').val("-1");		
						
						$('#divResultado').removeClass("d-none");
						$('#selectResultado').val("POSITIVO");
						
					}else{
						$('#divExamenes').removeClass("d-none");
						$('#divExamenesGrupal').addClass("d-none");
						$('#selectExamenes').val("-1");
						
						$('#divResultado').addClass("d-none");
						$('#selectResultado').val("POSITIVO");
					}	
					
					
				}
			);
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	
	$('body').on(
			'change',
			'#selectMarca',
			function(){
				if($('#selectMarca').val() > 0){
					var ajax = new AjaxUtil();
					ajax.sendRequest('get', _contextPath + 'dashboard-covid/get-convenios/'
							+ $('#selectMarca').val(), null,
							_bitacoraDashboard.fillOptionsConvenios,
							_javascriptTemplate.redirectError);
					
					
					ajax.sendRequest('get', _contextPath + 'dashboard-sucursal-marca/'
							+ $('#selectMarca').val(), null,
							_bitacoraDashboard.fillOptionsSucursal,
							_javascriptTemplate.redirectError);
					
				}else{
					var options;
					options += '<option selected value="-1">Todas</option>';
					$('#selectConvenio').empty().append(options);
					$('#selectConvenio').prop('disabled', true);
					
					
					
					var options2;
					options2 += '<option value="-1">Todas</option>';
					$('#selectSucursal').empty().append(options2);
					$('#selectSucursal').prop('disabled', true);
					
				}
			}
		);
	

	this.fillOptionsConvenios = function(data) {
//		console.log(data);
		var options;
		options += '<option selected value="-1">Todos</option>';
		$.each(data, function(i, item) {
			options += '<option value=' + item.cconvenio + '>' + item.cconvenio +' - '+ item.sconvenio+ '</option>'
		});

		$('#selectConvenio').empty().append(options);
//		$('#selectConvenio2').empty().append(options);
		$('#selectConvenio').prop('disabled', false);
	}
	
	
	this.fillOptionsSucursal = function(data) {
		var options;
		options += '<option value="-1">Todas</option>';
		$.each(data, function(i, item) {
			options += '<option value=' + item.csucursal + '>' + item.snombresucursal + '</option>'
		});

		$('#selectSucursal').empty().append(options);
		$('#selectSucursal').prop('disabled', false);
	}
	
	
	
	
	
	
	
	$('body')
	.on(
			'click',
			'#buttonLimpiar',
			function() {
				
				location.reload();
				
//				$('#selectMarca').val(-1);
//				var options;
//				options += '<option value="-1">Todas</option>';
//				$('#selectConvenio').empty().append(options);
//				$('#selectConvenio').prop('disabled', true);
//				
//				var options2;
//				options2 += '<option value="-1">Todas</option>';
//				$('#selectSucursal').empty().append(options2);
//				$('#selectSucursal').prop('disabled', true);
//				
//				$('#selectLiberada').val(1);
//				$('#selectFechaLiberacion').val("");
//				$('#selectFechaCaptura').val("");
//				$('#inputOrden').val("");
//				$('#inputConsecutivo').val("");
//				
//				$('#inputNombre').val("");
//				$('#inputPaterno').val("");
//				$('#inputMaterno').val("");
//				$("#tableResultadosDetalle tbody").html("");
	});
	
	
	
	
//	$('#inputCheckin').keyup( function() {
//	      var a = $("#inputCheckin" ).val();
//		console.log(a);
//	    })
	
	$("#inputCheckin").keypress(function(e) {
        if(e.which == 13) {
        	 var a = $("#inputCheckin" ).val();
        	console.log(a);
        	
        	var ajax = new AjaxUtil();
			ajax.sendRequest('get', _contextPath + 'dashboard-covid/procesar-tracking-muestra/'+a, null,
					_bitacoraDashboard.successCheckingMuestra,
					_bitacoraDashboard.errorCheckingMuestra);		
        	
        }
      });
	
	
	
	this.successCheckingMuestra = function(data){
		console.log(data);
		if(data.estatus =='Error'){
			$('#msgDefaultError').empty();
			$('#msgDefaultError').html('</br></br><h6> Err: '
							+ data.descripcion + '</h6></br>');
			$('#modalError').modal('show');
		}else{			
			$('#msgDefaultSuccess').empty();
			$('#modalSuccess').modal('show');
		}
	}
	
	this.errorCheckingMuestra = function(data){
//		var responseTimbrado = $.parseJSON(data.responseText);
		$('#msgDefaultError').empty();
		$('#msgDefaultError').html('</br></br><h6> Err: '
						+ data.descripcion + '</h6></br>');
		$('#modalError').modal('show');
	}
	
	
	
	
	
	/*
	 * Evento: Click Muestra la gráfica
	 */
	$('body')
			.on(
					'click',
					'#buttonGuardarIncidencia',
					function() {
						var estatus = $('#selectIncidencia').val();
						var etiqueta = $('#inputEtiqueta').val();
						var ajax = new AjaxUtil();
						ajax.sendRequest('get', _contextPath + 'dashboard-covid/actualizar-incidencia-muestra/'+etiqueta+"/"+estatus, null,
								_bitacoraDashboard.successActualizarEstatus,
								_bitacoraDashboard.errorActualizarEstatus);		
	});
	
	
	this.successActualizarEstatus = function (data) {
		console.log(data);
		if(data.estatus =='Error'){
			$('#msgDefaultError').empty();
			$('#msgDefaultError').html('</br></br><h6> Err: '
							+ data.descripcion + '</h6></br>');
			$('#modalError').modal('show');
		}else{			
			$('#msgDefaultSuccess').empty();
			$('#modalSuccess').modal('show');
		}
	}
	
	this.errorActualizarEstatus = function (data) {
		$('#msgDefaultError').empty();
		$('#msgDefaultError').html('</br></br><h6> Err: '
						+ data.descripcion + '</h6></br>');
		$('#modalError').modal('show');
	}
	
	
	
	
	
	
	
	/*
	 * Evento: Click Muestra la gráfica
	 */
	$('body')
			.on(
					'click',
					'#buttonProcesar',
					function() {
						var cconvenio = $('#selectConvenio').val();
						
						var convenio = [];
						filtro = new FilterCovidDto(0, 0, convenio, '', '',
								0, "", "", "", "", 0,"","","","",0,0,
								0, 0, 0, "", true, '', cconvenio);
						
						
						var jsonFiltro = JSON.stringify(filtro);
						console.log(jsonFiltro);
						
						
						var ajax = new AjaxUtil();
						ajax.sendRequest('post', _contextPath + 'dashboard-eventos/get-reporte-evento', jsonFiltro,
								_bitacoraDashboard.successEstadisticaErrores,
								_bitacoraDashboard.errorReporteCovid);	
						
						
//						_selectedPdf = [];
//						_urlZipDescarga = null;
//						$('#buttonDownloadZip').addClass('d-none');
//						$('#buttonDownloadReporte').addClass('d-none');
//						var convenio = [];
//						var marca = $('#selectMarca').val();
//						var sucursal = $('#selectSucursal').val();
////						$('select').selectpicker();
////						var convenio = $('#selectConvenio').selectpicker();
//						convenio = $('#selectConvenio').val();
//						var liberada = $('#selectLiberada').val();
//						var fliberacion = $('#selectFechaLiberacion').val().trim();
//						var fcaptura = $('#selectFechaCaptura').val().trim();
//						var orden = $('#inputOrden').val().trim();
//						var consecutivo = $('#inputConsecutivo').val().trim();
//						var nombre = $('#inputNombre').val().trim();
//						var paterno = $('#inputPaterno').val().trim();
//						var materno = $('#inputMaterno').val().trim();
//						var opelibe = $('#selectOperacion').val();
//						var opecapt = $('#selectOperacion2').val();
//						var horaliberacion = $('#selectHoraLiberacion').val();
//						var horacaptura = $('#selectHoraCaptura').val();
//						
//						var nomuestra = $('#inputMuestra').val();
//						var ftoma = $('#selectFechaToma').val();
//						var opetoma = $('#selectOperacion3').val();
//						var muestratomada = $('#selectMuestraTomada').val();
//						var catalogounico = $('#selectExamenes').val();
//						var cexamen = $('#selectExamenGrupal').val();
//						var resultado = $('#selectResultado').val();
//						
//						console.log(fliberacion);
//						console.log(fcaptura);
//						console.log(horaliberacion);
//						console.log(horacaptura);
//						
//						if(horaliberacion!=""){
//							horaliberacion = horaliberacion + ':00'
//						}
//						if(horacaptura!=""){
//							horacaptura = horacaptura + ':00'
//						}
//						
////						var libe;
////						if(liberada == 1 ){
////							libe = true;
////						}else{
////							libe = false;
////						}
//						if(orden == ""){
//							orden = 0;
//						}
//						if(nomuestra == ""){
//							nomuestra = 0;
//						}
//						
//						var tipoexamen = 0;
//						if($('#checkGrupal').prop('checked')){
//							tipoexamen = 1;
//						}else{
//							tipoexamen = 2;
//						}	
//						
//						
//						
//					
//						filtro = new FilterCovidDto(marca, sucursal, convenio, fcaptura+' '+horacaptura, fliberacion+' '+horaliberacion,
//								orden, consecutivo, nombre, paterno, materno, liberada,opelibe,opecapt,opetoma,ftoma,nomuestra,muestratomada,
//								catalogounico, tipoexamen, cexamen, resultado, false, null,0);
//						
//						
//						var jsonFiltro = JSON.stringify(filtro);
//						console.log(jsonFiltro);
//						
//						
//						var ajax = new AjaxUtil();
//						ajax.sendRequest('post', _contextPath + 'dashboard-covid/get-reporte-covid', jsonFiltro,
//								_bitacoraDashboard.successEstadisticaErrores,
//								_bitacoraDashboard.errorReporteCovid);							
												
						
						
	});
	
	
	this.errorReporteCovid = function(data){
		console.log(data);
		if (!data.responseText) {
			console.log(data);
//			_javascriptTemplate.redirectError();
		} else {
			var responseTimbrado = $.parseJSON(data.responseText);			
			$('#msgDefaultError').empty();
			$('#msgDefaultError').html('</br></br><h6> Err: '
							+ responseTimbrado + '</h6></br>');
			$('#modalError').modal('show');			
		}
	}
	
	
	
	
	$('body')
			.on(
					'click',
					'#buttonZip',
					function() {
						$('#buttonDownloadZip').addClass('d-none');
						if(_selectedPdf.length > 0){
							var jsonFiltro = JSON.stringify(_selectedPdf);
//							console.log(jsonFiltro);
							
							var ajax = new AjaxUtil();
							ajax.sendRequest('post', _contextPath + 'dashboard-covid/get-pdf-result-full', jsonFiltro,
									_bitacoraDashboard.successZipFull,
									_bitacoraDashboard.successZipFullError);
						}
						
						
						
	});
	
	
	this.successZipFull = function(data){
		console.log(data);
		_urlZipDescarga = data;
		$('#buttonDownloadZip').removeClass('d-none');
		$('#msgDefaultSuccess').empty();
		$('#modalSuccess').modal('show');
	}
	
	this.successZipFullError = function(data){
		console.log(data);
		if (!data.responseText) {
			_javascriptTemplate.redirectError();
		} else {
			var responseTimbrado = $.parseJSON(data.responseText);			
			$('#msgDefaultError').empty();
			$('#msgDefaultError').html('</br></br><h6> Err: '
							+ data + '</h6></br>');
			$('#modalError').modal('show');			
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	$('body')
	.on(
			'click',
			'#buttonGenerarZipMasivo',
			function() {
					$('#buttonDownloadZipMasivo').addClass('d-none');
					var consecutivo = $('#inputConsecutivo').val().trim();
					if(consecutivo!=''){
						var jsonFiltro = JSON.stringify(consecutivo);
	//					console.log(jsonFiltro);
						
						var ajax = new AjaxUtil();
						ajax.sendRequest('post', _contextPath + 'dashboard-covid/get-pdf-result-consecutivos-full', consecutivo,
								_bitacoraDashboard.successZipFullMasivo,
								_bitacoraDashboard.errorZipFullMasivo);
					}
					
					
					
	});


	this.successZipFullMasivo = function(data){
		console.log(data);
		_urlZipDescargaMasiva = data;
		$('#buttonDownloadZipMasivo').removeClass('d-none');
		$('#msgDefaultSuccess').empty();
		$('#modalSuccess').modal('show');
	}

	this.errorZipFullMasivo = function(data){
		console.log(data);
		if (!data.responseText) {
			_javascriptTemplate.redirectError();
		} else {
			var responseTimbrado = $.parseJSON(data.responseText);			
			$('#msgDefaultError').empty();
			$('#msgDefaultError').html('</br></br><h6> Err: '
							+ data + '</h6></br>');
			$('#modalError').modal('show');			
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	$('body')
	.on(
			'click',
			'#buttonDownloadZip',
			function() {
				
				var pdf_link = _urlZipDescarga;
				
				var href = $('#achorDescargaZip').attr('href');
				$('#achorDescargaZip').attr('href', href.replace('{url}', pdf_link));
				$('#achorDescargaZip')[0].click();
				_bitacoraDashboard.restoreAnchorZip();


			});
	
	this.restoreAnchorZip = function() {
		$('#achorDescargaZip').attr('href', '{url}');
	}
	
	
	
	$('body')
	.on(
			'click',
			'#buttonDownloadZipMasivo',
			function() {
				
				var pdf_link = _urlZipDescargaMasiva;
				
				var href = $('#achorDescargaZipMasivo').attr('href');
				$('#achorDescargaZipMasivo').attr('href', href.replace('{url}', pdf_link));
				$('#achorDescargaZipMasivo')[0].click();
				_bitacoraDashboard.restoreAnchorZipMasivo();


			});
	
	this.restoreAnchorZipMasivo = function() {
		$('#achorDescargaZipMasivo').attr('href', '{url}');
	}
	
	
	
	$('body')
	.on(
			'click',
			'#buttonDownloadReporte',
			function() {
				var convenio = [];
				var marca = $('#selectMarca').val();
				var sucursal = $('#selectSucursal').val();
//				$('select').selectpicker();
//				var convenio = $('#selectConvenio').selectpicker();
				convenio = $('#selectConvenio').val();
				var liberada = $('#selectLiberada').val();
				var fliberacion = $('#selectFechaLiberacion').val().trim();
				var fcaptura = $('#selectFechaCaptura').val().trim();
				var orden = $('#inputOrden').val().trim();
				var consecutivo = $('#inputConsecutivo').val().trim();
				var nombre = $('#inputNombre').val().trim();
				var paterno = $('#inputPaterno').val().trim();
				var materno = $('#inputMaterno').val().trim();
				var opelibe = $('#selectOperacion').val();
				var opecapt = $('#selectOperacion2').val();
				var horaliberacion = $('#selectHoraLiberacion').val();
				var horacaptura = $('#selectHoraCaptura').val();
				
				var nomuestra = $('#inputMuestra').val();
				var ftoma = $('#selectFechaToma').val();
				var opetoma = $('#selectOperacion3').val();
				var muestratomada = $('#selectMuestraTomada').val();
				var catalogounico = $('#selectExamenes').val();
				var cexamen = $('#selectExamenGrupal').val();
				var resultado = $('#selectResultado').val();
				
				console.log(fliberacion);
				console.log(fcaptura);
				console.log(horaliberacion);
				console.log(horacaptura);
				
				if(horaliberacion!=""){
					horaliberacion = horaliberacion + ':00'
				}
				if(horacaptura!=""){
					horacaptura = horacaptura + ':00'
				}
				
//				var libe;
//				if(liberada == 1 ){
//					libe = true;
//				}else{
//					libe = false;
//				}
				if(orden == ""){
					orden = 0;
				}
				if(nomuestra == ""){
					nomuestra = 0;
				}
				
				var tipoexamen = 0;
				if($('#checkGrupal').prop('checked')){
					tipoexamen = 1;
				}else{
					tipoexamen = 2;
				}
				
			
//				filtro = new FilterCovidDto(marca, sucursal, convenio, fcaptura+' '+horacaptura, fliberacion+' '+horaliberacion,
//						orden, consecutivo, nombre, paterno, materno, liberada,opelibe,opecapt,opetoma,ftoma,nomuestra,muestratomada);
//				
//				var ajax = new AjaxUtil();
//				ajax.sendRequest('get', _contextPath + 'dashboard-covid/prueba?marca='+marca+'&sucursal='+sucursal+'&convenio='+convenio+
//						'&fcaptura='+fcaptura+' '+horacaptura+'&fliberacion='+fliberacion+' '+horaliberacion+'&orden='+orden+
//						'&consecutivo='+consecutivo+'&nombre='+nombre+'&paterno='+paterno+'&materno='+materno+'&liberada='+liberada+
//						'&operliberacion='+operliberacion+'&opercaptura='+opercaptura+'&opertoma='+opertoma+'&ftoma='+ftoma+
//						'&nummuestra='+nummuestra+'&muestratomada='+muestratomada, null,
//						_bitacoraDashboard.successDescargarReporte,
//						_bitacoraDashboard.errorDescargarReporte);
				
				var reporte_link = 'dashboard-covid/reporte-excel-covid?marca='+marca+'&sucursal='+sucursal+'&convenio='+convenio+
				'&fcaptura='+fcaptura+' '+horacaptura+'&fliberacion='+fliberacion+' '+horaliberacion+'&orden='+orden+
				'&consecutivo='+consecutivo+'&nombre='+nombre+'&paterno='+paterno+'&materno='+materno+'&liberada='+liberada+
				'&operliberacion='+opelibe+'&opercaptura='+opecapt+'&opertoma='+opetoma+'&ftoma='+ftoma+
				'&nummuestra='+nomuestra+'&muestratomada='+muestratomada+'&catalogounico='+catalogounico+
				'&tipoexamen='+tipoexamen+'&cexamen='+cexamen+'&resultado='+resultado;
				
				var href = $('#achorDescargaReporteCovid').attr('href');
				$('#achorDescargaReporteCovid').attr('href', href.replace('{url}', reporte_link));
				$('#achorDescargaReporteCovid')[0].click();
				_bitacoraDashboard.restoreAnchorReporteCovid();

				
			});	
	
	this.restoreAnchorReporteCovid = function() {
		$('#achorDescargaReporteCovid').attr('href', '{url}');
	}
	
	
	
	
	
	
	
	$('body').on('click', '#btnDefaultSucceess', function() {
		$('#modalSuccess').modal('hide');
	});

	/**
	 * Oculta el modal de error
	 * 
	 */
	$('body').on('click', '#btnDefaultError', function() {
		$('#modalError').modal('hide');
	});
	
	
	$('body').on('click', '.btn-descarga-pdf', function() {
		var orden = $(this).attr('id');
		var nombrepaciente = $(this).attr('name');
		var ssucursal = $(this).attr('ssucursal');
		
		var ajax = new AjaxUtil();
		ajax.sendRequest('get', _contextPath + 'dashboard-covid/get-pdf-result/'+orden+'/'+nombrepaciente+'/'+ssucursal, null,
				_bitacoraDashboard.successPDF,
				_bitacoraDashboard.successPDFError);
		
		
		
		
//			var href = $('#achorDescargaPdf').attr('href');
//			$('#achorDescargaPdf').attr('href', href.replace('{orden}', orden).replace('{paciente}', nombrepaciente));
//			$('#achorDescargaPdf')[0].click();
//			_reasignacion.restoreAnchor();
			
	});
	
	
	
	
	
	
	$('body').on('click', '.btn-crear-detalle', function() {
//		var idBtn = $(this).attr('id');
		$(this).prop("disabled", true );
		var consecutivo = $(this).attr('name');	
		var ajax = new AjaxUtil();
		ajax.sendRequest('post', _contextPath + 'dashboard-covid/crear-detalle/'+consecutivo, null,
				_bitacoraDashboard.successCrearDetalle,
				_bitacoraDashboard.errorCrearDetalle);
		
			
	});
	
	
	this.successCrearDetalle = function(data){
		console.log(data);				
		$('#msgDefaultSuccess').empty();
		$('#msgDefaultSuccess').html('</br><h6>'
				+ data + '</h6>');
		$('#modalSuccess').modal('show');
	}
	
	this.errorCrearDetalle = function(data){
		var responseTimbrado = $.parseJSON(data.responseText);
		$('#msgDefaultError').empty();
		$('#msgDefaultError').html('</br><h6> Err: '
						+ responseTimbrado + '</h6></br>');
		$('#modalError').modal('show');
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	this.successPDF = function(data){
		console.log(data);
		var pdf_link = data;
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>'
        $.createModal({
        title:'PDF',
        message: iframe,
        closeButton:true,
        scrollable:true
        });
        return false;  
	}
	
	this.successPDFError = function(data){
		console.log(data);
	}
	
	
	
	
	
	$('body').on('click', '#buttonPro', function() {
		var orden = $(this).attr('id');
		var nombrepaciente = $(this).attr('name');
		var pdf_link = 'http://10.20.26.6:9085/ReporteResult/ResultadosZip-06052020123230.zip';
		
		var href = $('#achorDescargaZip').attr('href');
		$('#achorDescargaZip').attr('href', href.replace('{url}', pdf_link));
		$('#achorDescargaZip')[0].click();
		_bitacoraDashboard.restoreAnchor();
		
			
	});
	
	
	this.restoreAnchor = function(oldAnchor) {
		$('#achorDescargaZip').attr('href', '{url}');
	}
	
	
	
	
	/*
	 * Evento: Click Muestra la gráfica
	 */
	$('body')
			.on(
					'click',
					'#buttonGenerar',
					function() {
//						_bitacoraDashboard.limpiarAll();
						$('#divDetalles').removeClass('d-none');
						$('#divPendientes').addClass('d-none');
						var fInicial= $('#selectFechaI').val();
						
						if(fInicial!=""){
							$('#inputErrorfInicial').addClass('d-none');
							var ajax = new AjaxUtil();
							ajax.sendRequest('get', _contextPath + 'dashboard-error/get-estadistica?fInicial='+fInicial, null,
									_bitacoraDashboard.successReporteCovid,
									_javascriptTemplate.redirectError);
						}else{
							$('#inputErrorfInicial').removeClass('d-none');
						}
						
						
	});
	
	this.successReporteCovid = function(data){		
		_bitacoraDashboard.loadDataTable(data);
	}
	
	
	
	
	
	$('body')
	.on(
			'click',
			'#buttonPendientes',
			function() {
//				_bitacoraDashboard.limpiarAll();}
				$('#divDetalles').addClass('d-none');
				$('#divPendientes').removeClass('d-none');
				var fInicial= $('#selectFechaI').val();
				
				if(fInicial!=""){
					$('#inputErrorfInicial').addClass('d-none');
					var ajax = new AjaxUtil();
					ajax.sendRequest('get', _contextPath + 'dashboard-error/get-pendientes-facturacion?fInicial='+fInicial, null,
							_bitacoraDashboard.successEstadisticaPendientes,
							_javascriptTemplate.redirectError);
				}else{
					$('#inputErrorfInicial').removeClass('d-none');
				}				
});
	
	
	
	this.successEstadisticaPendientes = function(data){		
		_bitacoraDashboard.loadDataTablePendientes(data);
	}
	
	
	this.successEstadisticaErrores = function(data){
		console.log(data);
//		$('#buttonDownloadReporte').removeClass('d-none');
		_listReportesPdf = data.listinfopdf;
		_objReporteCovid = data;
		_bitacoraDashboard.loadDataTable(data.listreporte);
		
//		_bitacoraDashboard.loadDataTable(data);
	}
	
	
	
	this.limpiarGrafica = function(){
		$("#canvas-holder").html("");
		
		canvas = '<canvas id="chart-estadistica"></canvas>';
		$(canvas).appendTo("#canvas-holder");
	}
	
	this.limpiarAll = function(){
		_estadisticaData=null;
		$("#canvas-holder").html("");
		
		canvas = '<canvas id="chart-estadistica"></canvas>';
		$(canvas).appendTo("#canvas-holder");
		
		
		$("#tableEstadistica tbody").html("");
		
		rows='<tr>'
		+'<th scope="row">Examenes Prometidos</th>'
		+'<td class="text-center align-middle" id="vPrometidos">-</td>'
		+'<td class="text-center align-middle" id="pPrometidos">-</td>'
		+'</tr>'
		+'<tr>'
		+'<th scope="row">Liberados</th>'
		+'<td class="text-center align-middle" id="vLiberados">-</td>'
		+'<td class="text-center align-middle" id="pLiberados">-</td>'
		+'</tr>'
		+'<tr>'
		+'<th scope="row">Email con Estructura Correcta</th>'
		+'<td class="text-center align-middle" id="vEmail">-</td>'
		+'<td class="text-center align-middle" id="pEmail">-</td>'
		+'</tr>'
		+'<tr>'
		+'<th scope="row">Creación del PDF</th>'
		+'<td class="text-center align-middle" id="vPdf">-</td>'
		+'<td class="text-center align-middle" id="pPdf">-</td>'
		+'</tr>'
		+'<tr>'
		+'<th scope="row">Entregados</th>'
		+'<td class="text-center align-middle" id="vEntregados">-</td>'
		+'<td class="text-center align-middle" id="pEntregados">-</td>'
		+'</tr>'
		+'<tr>'
		+'<th scope="row">Leídos por el Paciente</th>'
		+'<td class="text-center align-middle" id="vLeidos">-</td>'
		+'<td class="text-center align-middle" id="pLeidos">-</td>'
		+'</tr>';
		$(rows).appendTo("#tableEstadistica tbody");	
		
//		var canvas = document.getElementById("chart-estadistica");
//		var ctx = canvas.getContext('2d');
//		ctx.clearRect(0, 0, canvas.width, canvas.height);
	}
	
	this.successValoresEstadistica = function(data){
		console.log(data);
		_estadisticaData = data;
		var ptotalordenes = 0;
		var pcompletos = 0;
		var pparciales = 0;
		
		if(data.totalOrdenes != 0){
			ptotalordenes = 100;
		}
		
		if(data.ordenesCompletas != 0){
			pcompletos = ((data.ordenesCompletas*100)/data.totalOrdenes);
			if((pcompletos%2)!= 0 && (pcompletos%2)!= 1){
				pcompletos=pcompletos.toFixed(2);
			}
		}
		
		if(data.ordenesParciales != 0){
			pparciales = ((data.ordenesParciales*100)/data.totalOrdenes);
			if((pparciales%2)!= 0 && (pparciales%2)!= 1){
				pparciales=pparciales.toFixed(2);
			}
		}
		
		
		var presultadosEnviados = 0;
		var presultadoEnProceso = 0;
		var presultadoNoEnviados = 0;
		var ptotalResultados = 0;
		
		if(data.totalEstudios != 0){
			ptotalResultados = 100;
		}
		if(data.totalEstudiosEnviados != 0){
			presultadosEnviados = ((data.totalEstudiosEnviados*100)/data.totalEstudios);
			if((presultadosEnviados%2)!= 0 && (presultadosEnviados%2)!= 1){
				presultadosEnviados=presultadosEnviados.toFixed(2);
			}
		}
		if(data.totalEstudiosEnProceso != 0){
			presultadoEnProceso = ((data.totalEstudiosEnProceso*100)/data.totalEstudios);
			if((presultadoEnProceso%2)!= 0 && (presultadoEnProceso%2)!= 1){
				presultadoEnProceso=presultadoEnProceso.toFixed(2);
			}
		}
		var resultadosNoEnviados = data.totalEstudios - (data.totalEstudiosEnviados + data.totalEstudiosEnProceso);
		if(resultadosNoEnviados != 0){
			presultadoNoEnviados = ((resultadosNoEnviados*100)/data.totalEstudios);
			if((presultadoNoEnviados%2)!= 0 && (presultadoNoEnviados%2)!= 1){
				presultadoNoEnviados=presultadoNoEnviados.toFixed(2);
			}
		}
		
		
		
		$("#tableEstadistica tbody").html("");
		
		rows='<tr>'
		+'<th scope="row">Ordenes Completas</th>'
		+'<td class="text-center align-middle" id="vPrometidos">'+data.ordenesCompletas+'</td>'
		+'<td class="text-center align-middle" id="pPrometidos">'+pcompletos+'</td>'
		+'</tr>'
		+'<tr>'
		+'<th scope="row">Ordenes Parciales</th>'
		+'<td class="text-center align-middle" id="vLiberados">'+data.ordenesParciales+'</td>'
		+'<td class="text-center align-middle" id="pLiberados">'+pparciales+'</td>'
		+'</tr>'
		+'<tr class="table-primary">'
		+'<th scope="row">Total de Ordenes</th>'
		+'<td class="text-center align-middle" id="vEmail">'+data.totalOrdenes+'</td>'
		+'<td class="text-center align-middle" id="pEmail">'+ptotalordenes+'</td>'
		+'</tr>'
		$(rows).appendTo("#tableEstadistica tbody");	
		
		
		$("#tableEstadisticaResultados tbody").html("");
		
		rows='<tr>'
		+'<th scope="row"><button id="btnVerResulEnviados" type="button" class="btn btn-link">Resultados Enviados</button></th>'
		+'<td class="text-center align-middle" id="vPrometidos">'+data.totalEstudiosEnviados+'</td>'
		+'<td class="text-center align-middle" id="pPrometidos">'+presultadosEnviados+'</td>'
		+'</tr>'
		+'<tr>'
		+'<th scope="row"><button id="btnVerResulEnProceso" type="button" class="btn btn-link">Resultados En Proceso</button></th>'
		+'<td class="text-center align-middle" id="vLiberados">'+data.totalEstudiosEnProceso+'</td>'
		+'<td class="text-center align-middle" id="pLiberados">'+presultadoEnProceso+'</td>'
		+'</tr>'
		+'<tr>'
		+'<th scope="row"><button id="btnVerResulNoEnviados" type="button" class="btn btn-link">Resultados No Enviados</button></th>'
		+'<td class="text-center align-middle" id="vLiberados">'+resultadosNoEnviados+'</td>'
		+'<td class="text-center align-middle" id="pLiberados">'+presultadoNoEnviados+'</td>'
		+'</tr>'
		+'<tr class="table-primary">'
		+'<th scope="row">Total de Resultados</th>'
		+'<td class="text-center align-middle" id="vEmail">'+data.totalEstudios+'</td>'
		+'<td class="text-center align-middle" id="pEmail">'+ptotalResultados+'</td>'
		+'</tr>'
		$(rows).appendTo("#tableEstadisticaResultados tbody");	
		
		
		
		
	}
	
	
	
	
	$('body')
	.on(
			'click',
			'#btnVerResulEnviados',
			function() {				
				_bitacoraDashboard.loadDataTable(_estadisticaData.listEnviados);
			});
	
	
	$('body')
	.on(
			'click',
			'#btnVerResulEnProceso',
			function() {
				_bitacoraDashboard.loadDataTable(_estadisticaData.listEnProceso);
			});
	
	
	$('body')
	.on(
			'click',
			'#btnVerResulNoEnviados',
			function() {
				_bitacoraDashboard.loadDataTable(_estadisticaData.listNoEnviados);
			});
	
	
	
	
	
	$('body').on('change', '.check-generar-pdf', function() {		
		var checkedPdf = $(this).is(':checked');
		var orden = $(this).attr('id');
		var paciente = $(this).attr('name');
		var ordenPaciente = orden+'|'+paciente;
		if(checkedPdf){
			_selectedPdf.push(ordenPaciente);
		}else{
			var indexOfItem = _selectedPdf.findIndex(function (numIdPdf){
				return numIdPdf === ordenPaciente;
			});
			
			_selectedPdf.splice(indexOfItem, 1);
		}
//		console.log(_selectedPdf);
	});
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	this.loadDataTablePendientes = function(jsonPagos) {
		if (_tablePendientes != null) {
			$("#bodyPendientes").empty();
			_tablePendientes.destroy();
		}
		_tablePendientes = $('#tablePendientes')
				.DataTable(
						{
							"data" : jsonPagos,
							"language" : {
								"url" : "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
							},
							"columns" : [
									{
										"data" : "kordensucursal",
										"render" : function(data, type, full,
												meta) {
											return '<div class="text-center" style="font-weight: bold;">'
													+ data + '</div>';
										}
									},
									{
										"data" : "uorden",
										"render" : function(data, type, full,
												meta) {
											return '<div class="text-center" style="font-weight: bold;">'
													+ data + '</div>';
										}
									},
									{
										"data" : "marca",
										"render" : function(data, type, full,
												meta) {
											return '<div class="text-center" style="font-weight: bold;">'
													+ data + '</div>';
										}

									},
									{
										"data" : "mpagopaciente",
										"render" : function(data, type, full,
												meta) {
											return '<div class="text-center" style="font-weight: bold;">'
													+ data + '</div>';
										}
									},
									{
										"data" : "mpagopacienteiva",
										"render" : function(data, type, full,
												meta) {
											return '<div class="text-center" style="font-weight: bold;">'
											+ data + '</div>';
										}
									},
									{
										"data" : "dregistro",
										"render" : function(data, type, full,
												meta) {
											return '<div class="text-center" style="font-weight: bold;">'
											+ data + '</div>';
										}
									} ],
							"order" : [ [ 0, "desc" ] ],
						});
	}
	
	
	
	
	
	
	
	
	
	
	
	
	$('body').on(
			'click',
			'.button-ver-log',
			function() {
				
				ajax = new AjaxUtil();
				ajax.sendRequest('get', _contextPath
						+ 'dashboard-error/get-log-error/'
						+ $(this).attr('id'), null, _bitacoraDashboard.showLogError,
						_javascriptTemplate.redirectError);
				
			});
	
	
	this.showLogError = function(data) {
		$('#msgLog').empty();
		$('#msgLog').html("<xmp>"+data+"</xmp>");
		$('#modalLog').modal('show');
	}
	
	
	$('body').on(
			'click',
			'.button-ver-mensaje',
			function() {
				
				ajax = new AjaxUtil();
				ajax.sendRequest('get', _contextPath
						+ 'dashboard-error/get-mensaje-entrada/'
						+ $(this).attr('id'), null, _bitacoraDashboard.showMensajeEntrada,
						_javascriptTemplate.redirectError);
				
			});
	
	
	this.showMensajeEntrada = function(data) {
		$('#msgMensajeEntrada').empty();
		$('#msgMensajeEntrada').html("<xmp>"+data+"</xmp>");
		$('#modalMensaje').modal('show');
	}
	
	
	$('body').on(
			'click',
			'.button-ver-mensaje-gda',
			function() {
				
				ajax = new AjaxUtil();
				ajax.sendRequest('get', _contextPath
						+ 'dashboard-error/get-mensaje-gda/'
						+ $(this).attr('id'), null, _bitacoraDashboard.showMensajeEntradaGDA,
						_javascriptTemplate.redirectError);
				
			});
	
	
	this.showMensajeEntradaGDA = function(data) {
		$('#msgMensajeEntradaGda').empty();
		$('#msgMensajeEntradaGda').html("<xmp>"+data+"</xmp>");
		$('#modalMensajeGda').modal('show');
	}
	
	
	
	this.validarCampos = function(marca,finicial,ffinal){
		var validacion= true;
		
		if(marca==-1){
			validacion = false;
			$('#inputErrorMarca').removeClass('d-none');
		}else{
			$('#inputErrorMarca').addClass('d-none');
		}
		if(finicial == ""){
			validacion = false;
			$('#inputErrorfInicial').removeClass('d-none');
		}else{
			$('#inputErrorfInicial').addClass('d-none');
		}
		if (ffinal == ""){
			validacion = false;
			$('#inputErrorfFinal').removeClass('d-none');
		}else{
			$('#inputErrorfFinal').addClass('d-none');
		}
		
		return validacion;		
	}
	
	
	$('body').on('click','#buttonLinea',function() {
		if(_estadisticaData != null){
			_bitacoraDashboard.limpiarGrafica();
			_bitacoraDashboard.graficaLine(_estadisticaData);					
		}
								
	});
	
	$('body').on('click','#buttonBarras',function() {
		if(_estadisticaData != null){
			_bitacoraDashboard.limpiarGrafica();
			_bitacoraDashboard.graficaBar(_estadisticaData);
		}
	});
	
	$('body').on('click','#buttonPolar',function() {
		if(_estadisticaData != null){
			_bitacoraDashboard.limpiarGrafica();
			_bitacoraDashboard.graficaPolar(_estadisticaData);
		}
	});
	
	this.graficaLine = function(data){
		
		var pprometidos =0;
		var pliberados = 0;
		var pCapturados = 0;
		var pPdf = 0;
		var pEntregados = 0;
		var pLeidos = 0;
		
		if(data.PROMETIDOS != 0){
			pprometidos = 100;
		}		
		if(data.COUNT_LIBERADOS != 0){
			pliberados = ((data.COUNT_LIBERADOS*100)/data.PROMETIDOS);
			if((pliberados%2)!= 0 && (pliberados%2)!= 1){
				pliberados=pliberados.toFixed(2);
			}
		}
		if(data.COUNT_CON_CORREO != 0){
			pCapturados = ((data.COUNT_CON_CORREO*100)/data.PROMETIDOS);
			if((pCapturados%2)!= 0 && (pCapturados%2)!= 1){
				pCapturados=pCapturados.toFixed(2);
			}
		}
		if(data.COUNT_SIN_ERROR != 0){
			pPdf =((data.COUNT_SIN_ERROR*100)/data.PROMETIDOS);
			if((pPdf%2)!= 0 && (pPdf%2)!= 1){
				pPdf=pPdf.toFixed(2);
			}
		}
		if(data.COUNT_ENTREGADOS != 0){
			pEntregados =((data.COUNT_ENTREGADOS*100)/data.PROMETIDOS);
			if((pEntregados%2)!= 0 && (pEntregados%2)!= 1){
				pEntregados=pEntregados.toFixed(2);
			}
		}
		if(data.COUNT_LEIDOS != 0){
			pLeidos = ((data.COUNT_LEIDOS*100)/data.PROMETIDOS);
			if((pLeidos%2)!= 0 && (pLeidos%2)!= 1){
				pLeidos=pLeidos.toFixed(2);
			}
		}
			
		var ctx = document.getElementById("chart-estadistica").getContext('2d');
			
		var myChart = new Chart(ctx,{
			type:'line',
			data:{
				labels: ["Examenes Prometidos","Liberados", "Email con Estructura Correcta", "Creación del PDF", "Entregados", "Leídos por el Paciente"],
				datasets:[{
					label: 'Porcentaje',
					data: [pprometidos,pliberados,pCapturados,pPdf,pEntregados,pLeidos],
					backgroundColor: [
					'rgba(0,0,0,0)'
					],
					borderColor: [
	              	'rgba(0, 0, 0, 0.6)'         
					],
					borderWidth:[2],
					pointBackgroundColor:[
					'rgba( 0, 81, 140, 0.8)',
					'rgba(27, 200, 86, 0.8)',
					'rgba(54, 162, 235, 0.8)',
					'rgba(255, 206, 86, 0.8)',
					'rgba(75, 192, 192, 0.8)',
					'rgba(255, 99, 132, 0.8)'
                    ],
                    pointBorderColor:[
					'rgba( 0, 81, 140, 0.9)',
					'rgba(27, 200, 86, 0.9)',
					'rgba(54, 162, 235, 0.9)',
					'rgba(255, 206, 86, 0.9)',
					'rgba(75, 192, 192, 0.9)',
					'rgba(255, 99, 132, 0.9)'
                    ],
                    pointBorderWidth:[1,1,1,1,1,1],
                    pointHitRadius:[5],
                    lineTension:[0],
                    pointHoverRadius:[8,8,8,8,8,8],
                    pointRadius:[6,6,6,6,6,6]
				}]
			},
			 options: {
			        scales: {
			            yAxes: [{
			                ticks: {
			                    beginAtZero:true
			                }
			            }],
					    xAxes: [{
					        ticks: {
					          autoSkip: false
					        }
					      }]
			        }
			    }
		});
	}
	
	
	this.graficaBar = function(data){
		var pprometidos =0;
		var pliberados = 0;
		var pCapturados = 0;
		var pPdf = 0;
		var pEntregados = 0;
		var pLeidos = 0;
		
		if(data.PROMETIDOS != 0){
			pprometidos = 100;
		}		
		if(data.COUNT_LIBERADOS != 0){
			pliberados = ((data.COUNT_LIBERADOS*100)/data.PROMETIDOS);
			if((pliberados%2)!= 0 && (pliberados%2)!= 1){
				pliberados=pliberados.toFixed(2);
			}
		}
		if(data.COUNT_CON_CORREO != 0){
			pCapturados = ((data.COUNT_CON_CORREO*100)/data.PROMETIDOS);
			if((pCapturados%2)!= 0 && (pCapturados%2)!= 1){
				pCapturados=pCapturados.toFixed(2);
			}
		}
		if(data.COUNT_SIN_ERROR != 0){
			pPdf =((data.COUNT_SIN_ERROR*100)/data.PROMETIDOS);
			if((pPdf%2)!= 0 && (pPdf%2)!= 1){
				pPdf=pPdf.toFixed(2);
			}
		}
		if(data.COUNT_ENTREGADOS != 0){
			pEntregados =((data.COUNT_ENTREGADOS*100)/data.PROMETIDOS);
			if((pEntregados%2)!= 0 && (pEntregados%2)!= 1){
				pEntregados=pEntregados.toFixed(2);
			}
		}
		if(data.COUNT_LEIDOS != 0){
			pLeidos = ((data.COUNT_LEIDOS*100)/data.PROMETIDOS);
			if((pLeidos%2)!= 0 && (pLeidos%2)!= 1){
				pLeidos=pLeidos.toFixed(2);
			}
		}
		
		var canvas = document.getElementById("chart-estadistica");
		var ctx = canvas.getContext('2d');					
		
		var myChart = new Chart(ctx, {
		    type: 'bar',
		    data: {
		        labels: ["Examenes Prometidos","Liberados", "Email con Estructura Correcta", "Creación del PDF", "Entregados", "Leídos por el Paciente"],
		        datasets: [{
		            label: 'Porcentaje',
		            data: [pprometidos,pliberados,pCapturados,pPdf,pEntregados,pLeidos],
		            backgroundColor: [
		                'rgba( 0, 81, 140, 0.7)',
		                'rgba(27, 200, 86, 0.7)',
		                'rgba(54, 162, 235, 0.7)',
		                'rgba(255, 206, 86, 0.7)',
		                'rgba(75, 192, 192, 0.7)',
		                'rgba(255, 99, 132, 0.7)'
		            ],
		            borderColor: [
		                'rgba( 0, 81, 140, 0.7)',
		                'rgba(27, 200, 86,1)',
		                'rgba(54, 162, 235, 1)',
		                'rgba(255, 206, 86, 1)',
		                'rgba(75, 192, 192, 1)',
		                'rgba(255, 99, 132, 1)'
		            ],
		            borderWidth: 1
		        }]
		    },
		    options: {
		        scales: {
		            yAxes: [{
		                ticks: {
		                    beginAtZero:true
		                }
		            }],
				    xAxes: [{
				        ticks: {
				          autoSkip: false
				        }
				      }]
		        }
		    }
		});
		
	}
	
	this.graficaPolar = function (data){
		
		var pprometidos =0;
		var pliberados = 0;
		var pCapturados = 0;
		var pPdf = 0;
		var pEntregados = 0;
		var pLeidos = 0;
		
		if(data.PROMETIDOS != 0){
			pprometidos = 100;
		}		
		if(data.COUNT_LIBERADOS != 0){
			pliberados = ((data.COUNT_LIBERADOS*100)/data.PROMETIDOS);
			if((pliberados%2)!= 0 && (pliberados%2)!= 1){
				pliberados=pliberados.toFixed(2);
			}
		}
		if(data.COUNT_CON_CORREO != 0){
			pCapturados = ((data.COUNT_CON_CORREO*100)/data.PROMETIDOS);
			if((pCapturados%2)!= 0 && (pCapturados%2)!= 1){
				pCapturados=pCapturados.toFixed(2);
			}
		}
		if(data.COUNT_SIN_ERROR != 0){
			pPdf =((data.COUNT_SIN_ERROR*100)/data.PROMETIDOS);
			if((pPdf%2)!= 0 && (pPdf%2)!= 1){
				pPdf=pPdf.toFixed(2);
			}
		}
		if(data.COUNT_ENTREGADOS != 0){
			pEntregados =((data.COUNT_ENTREGADOS*100)/data.PROMETIDOS);
			if((pEntregados%2)!= 0 && (pEntregados%2)!= 1){
				pEntregados=pEntregados.toFixed(2);
			}
		}
		if(data.COUNT_LEIDOS != 0){
			pLeidos = ((data.COUNT_LEIDOS*100)/data.PROMETIDOS);
			if((pLeidos%2)!= 0 && (pLeidos%2)!= 1){
				pLeidos=pLeidos.toFixed(2);
			}
		}
		
		
		var ctx = document.getElementById("chart-estadistica").getContext('2d');		
		
		var myChart = new Chart(ctx,{
			type:'polarArea',
			data:{
				labels: ["Examenes Prometidos (%)","Liberados (%)", "Email con Estructura Correcta (%)", "Creación del PDF (%)", "Entregados (%)", "Leídos por el Paciente (%)"],
				datasets:[{
					data: [pprometidos,pliberados,pCapturados,pPdf,pEntregados,pLeidos],
					backgroundColor: [
					'rgba( 0, 81, 140, 0.7)',
					'rgba(27, 200, 86, 0.7)',
					'rgba(54, 162, 235, 0.7)',
					'rgba(255, 206, 86, 0.7)',
					'rgba(75, 192, 192, 0.7)',
					'rgba(255, 99, 132, 0.7)'
					],
					borderAlign : ['center'],
					borderColor: [
					'rgba( 0, 81, 140, 0.8)',
					'rgba(27, 200, 86, 0.8)',
					'rgba(54, 162, 235, 0.8)',
					'rgba(255, 206, 86, 0.8)',
					'rgba(75, 192, 192, 0.8)',
					'rgba(255, 99, 132, 0.8)'         
					],
					borderWidth:[2]
				}]
			}
		});
	}
	
	
	this.iniciarGrafica = function() {
		var ctx = document.getElementById("chart-estadistica").getContext('2d');
		
		
		var myChart = new Chart(ctx, {
		    type: 'bar',
		    data: {
		        labels: ["Examenes Prometidos","Liberados", "Email con Estructura Correcta", "Creación del PDF", "Entregados", "Leídos por el Paciente"],
		        datasets: [{
		            label: 'Porcentaje',
		            data: [0,0,0,0,0,0],
		            backgroundColor: [
		                'rgba( 0, 81, 140, 0.7)',
		                'rgba(27, 200, 86, 0.7)',
		                'rgba(54, 162, 235, 0.7)',
		                'rgba(255, 206, 86, 0.7)',
		                'rgba(75, 192, 192, 0.7)',
		                'rgba(255, 99, 132, 0.7)'
		            ],
		            borderColor: [
		                'rgba( 0, 81, 140, 0.7)',
		                'rgba(27, 200, 86,1)',
		                'rgba(54, 162, 235, 1)',
		                'rgba(255, 206, 86, 1)',
		                'rgba(75, 192, 192, 1)',
		                'rgba(255, 99, 132, 1)'
		            ],
		            borderWidth: 1
		        }]
		    },
		    options: {
		        scales: {
		            yAxes: [{
		                ticks: {
		                    beginAtZero:true
		                }
		            }],
				    xAxes: [{
				        ticks: {
				          autoSkip: false
				        }
				      }]
		        }
		    }
		});
		
	}

	
}

function FilterCovidDto(marca, sucursal, convenio, fcaptura, fliberacion,
		orden, consecutivo, nombre, paterno, materno, liberada, operliberacion, 
		opercaptura,opertoma, ftoma, nummuestra, muestratomada, catalogounico, 
		tipoexamen, cexamen, resultado, bconsecutivo, sordenexterna, cconvenio) {
	this.marca = marca;
	this.sucursal = sucursal;
	this.convenio = convenio;
	this.fcaptura = fcaptura;
	this.fliberacion = fliberacion;
	this.orden = orden;
	this.consecutivo = consecutivo;
	this.nombre = nombre;
	this.paterno = paterno;
	this.materno = materno;
	this.liberada = liberada;
	this.operliberacion = operliberacion;
	this.opercaptura = opercaptura;
	this.opertoma = opertoma;
	this.ftoma = ftoma;
	this.nummuestra = nummuestra;
	this.muestratomada = muestratomada;
	this.catalogounico = catalogounico;
	this.tipoexamen = tipoexamen;
	this.cexamen = cexamen;
	this.resultado = resultado;
	this.bconsecutivo = bconsecutivo;
	this.sordenexterna = sordenexterna;
	this.cconvenio = cconvenio;
}

function InfoCovidDto(listreporte, listinfopdf){
	this.listreporte = listreporte;
	this.listinfopdf = listinfopdf;
}

function CrearPacienteDto(nombre, paterno, materno, sexo, nacimiento,
		correo, telefono, calle, colonia, ciudad, estado, pais,
		postal, convenio) {
	this.nombre = nombre;
	this.paterno = paterno;
	this.materno = materno;
	this.sexo = sexo;
	this.nacimiento = nacimiento;
	this.correo = correo;
	this.telefono = telefono;
	this.calle = calle;
	this.colonia = colonia;
	this.ciudad = ciudad;
	this.estado = estado;
	this.pais = pais;
	this.postal = postal;
	this.convenio = convenio;
}


function PacienteDto(id, nombre, paterno, materno, sexo, nacimiento,
		correo, telefono, calle, colonia, ciudad, estado, pais,
		postal, convenio, gruposanguineo, estadocivil, religion,
		base64imagen, marca, membresia) {
	this.id = id;
	this.nombre = nombre;
	this.paterno = paterno;
	this.materno = materno;
	this.sexo = sexo;
	this.nacimiento = nacimiento;
	this.correo = correo;
	this.telefono = telefono;
	this.calle = calle;
	this.colonia = colonia;
	this.ciudad = ciudad;
	this.estado = estado;
	this.pais = pais;
	this.postal = postal;
	this.convenio = convenio;
	this.gruposanguineo = gruposanguineo;
	this.estadocivil = estadocivil;
	this.religion = religion;
	this.base64imagen = base64imagen;
	this.marca = marca;
	this.membresia = membresia;
}



function HeredofamiliaresDto(diabetes, patentescoMamaDiabetes, patentescoPapaDiabetes,
		patentescoHnosDiabetes, patentescoAbuelosDiabetes, patentescoOtrosDiabetes,
		vasculares, patentescoMamaVasculares, patentescoPapaVasculares,
		patentescoHnosVasculares, patentescoAbuelosVasculares, patentescoOtrosVasculares,
		hipertension, patentescoMamaHipertension, patentescoPapaHipertension,
		patentescoHnosHipertension, patentescoAbuelosHipertension,
		patentescoOtrosHipertension, renales, patentescoMamaRenales,
		patentescoPapaRenales, patentescoHnosRenales, patentescoAbuelosRenales,
		patentescoOtrosRenales, reumatologico, patentescoMamaReumatologico,
		patentescoPapaReumatologico, patentescoHnosReumatologico,
		patentescoAbuelosReumatologico, patentescoOtrosReumatologico, cancer,
		patentescoMamaCancer, patentescoPapaCancer, patentescoHnosCancer,
		patentescoAbuelosCancer, patentescoOtrosCancer) {
	this.diabetes = diabetes;
	this.patentescoMamaDiabetes = patentescoMamaDiabetes;
	this.patentescoPapaDiabetes = patentescoPapaDiabetes;
	this.patentescoHnosDiabetes = patentescoHnosDiabetes;
	this.patentescoAbuelosDiabetes = patentescoAbuelosDiabetes;
	this.patentescoOtrosDiabetes = patentescoOtrosDiabetes;
	this.vasculares = vasculares;
	this.patentescoMamaVasculares = patentescoMamaVasculares;
	this.patentescoPapaVasculares = patentescoPapaVasculares;
	this.patentescoHnosVasculares = patentescoHnosVasculares;
	this.patentescoAbuelosVasculares = patentescoAbuelosVasculares;
	this.patentescoOtrosVasculares = patentescoOtrosVasculares;
	this.hipertension = hipertension;
	this.patentescoMamaHipertension = patentescoMamaHipertension;
	this.patentescoPapaHipertension = patentescoPapaHipertension;
	this.patentescoHnosHipertension = patentescoHnosHipertension;
	this.patentescoAbuelosHipertension = patentescoAbuelosHipertension;
	this.patentescoOtrosHipertension = patentescoOtrosHipertension;
	this.renales = renales;
	this.patentescoMamaRenales = patentescoMamaRenales;
	this.patentescoPapaRenales = patentescoPapaRenales;
	this.patentescoHnosRenales = patentescoHnosRenales;
	this.patentescoAbuelosRenales = patentescoAbuelosRenales;
	this.patentescoOtrosRenales = patentescoOtrosRenales;
	this.reumatologico = reumatologico;
	this.patentescoMamaReumatologico = patentescoMamaReumatologico;
	this.patentescoPapaReumatologico = patentescoPapaReumatologico;
	this.patentescoHnosReumatologico = patentescoHnosReumatologico;
	this.patentescoAbuelosReumatologico = patentescoAbuelosReumatologico;
	this.patentescoOtrosReumatologico = patentescoOtrosReumatologico;
	this.cancer = cancer;
	this.patentescoMamaCancer = patentescoMamaCancer;
	this.patentescoPapaCancer = patentescoPapaCancer;
	this.patentescoHnosCancer = patentescoHnosCancer;
	this.patentescoAbuelosCancer = patentescoAbuelosCancer;
	this.patentescoOtrosCancer = patentescoOtrosCancer;
}



function NoPatologicosDto(frecuenciaBano, frecuenciaDental, vacunacionCompleta,
		practicaDeporte, deporteFrecuencia, fuma, cigarrosPorSemana,
		dejoDeFumar, tomaAlcohol, edadInicio, alcoholFrecuencia,
		usadoDrogasCual, ultimaVezUsoDrogas) {
	this.frecuenciaBano = frecuenciaBano;
	this.frecuenciaDental = frecuenciaDental;
	this.vacunacionCompleta = vacunacionCompleta;
	this.practicaDeporte = practicaDeporte;
	this.deporteFrecuencia = deporteFrecuencia;
	this.fuma = fuma;
	this.cigarrosPorSemana = cigarrosPorSemana;
	this.dejoDeFumar = dejoDeFumar;
	this.tomaAlcohol = tomaAlcohol;
	this.edadInicio = edadInicio;
	this.alcoholFrecuencia = alcoholFrecuencia;
	this.usadoDrogasCual = usadoDrogasCual;
	this.ultimaVezUsoDrogas = ultimaVezUsoDrogas;
}



function PatologicosDto(asma, bronquitis, neumonia, tuberculosis,
		tosPersistente, faltaAire, gastritis, colitis, hepatitisTipoA,
		ulceraPeptica, lumbalgia, fracturas, esguinces, palpitaciones,
		fiebreReumatica, hipertesion, varices, traumaticos, desmayo,
		convulsiones, migraña, vertigo, manchas, onicomicosis,
		cantidadTatuajes, tatuajes, cantidadPerforaciones, perforaciones,
		diabetes, hipertiroidismo, hipotiroidismo, cirugias, transfunciones,
		padecimientoActual, fobias, alergias) {
	this.asma = asma;
	this.bronquitis = bronquitis;
	this.neumonia = neumonia;
	this.tuberculosis = tuberculosis;
	this.tosPersistente = tosPersistente;
	this.faltaAire = faltaAire;
	this.gastritis = gastritis;
	this.colitis = colitis;
	this.hepatitisTipoA = hepatitisTipoA;
	this.ulceraPeptica = ulceraPeptica;
	this.lumbalgia = lumbalgia;
	this.fracturas = fracturas;
	this.esguinces = esguinces;
	this.palpitaciones = palpitaciones;
	this.fiebreReumatica = fiebreReumatica;
	this.hipertesion = hipertesion;
	this.varices = varices;
	this.traumaticos = traumaticos;
	this.desmayo = desmayo;
	this.convulsiones = convulsiones;
	this.migraña = migraña;
	this.vertigo = vertigo;
	this.manchas = manchas;
	this.onicomicosis = onicomicosis;
	this.cantidadTatuajes = cantidadTatuajes;
	this.tatuajes = tatuajes;
	this.cantidadPerforaciones = cantidadPerforaciones;
	this.perforaciones = perforaciones;
	this.diabetes = diabetes;
	this.hipertiroidismo = hipertiroidismo;
	this.hipotiroidismo = hipotiroidismo;
	this.cirugias = cirugias;
	this.transfunciones = transfunciones;
	this.padecimientoActual = padecimientoActual;
	this.fobias = fobias;
	this.alergias = alergias;
}




function LaboralesDto(empleosPrevios, actividadesArea, antiguedad, jornadaLaboralSemanal,
		exposicionLaboral, tratamientosRecibidos, otrosTrabajos) {
	this.empleosPrevios = empleosPrevios;
	this.actividadesArea = actividadesArea;
	this.antiguedad = antiguedad;
	this.jornadaLaboralSemanal = jornadaLaboralSemanal;
	this.exposicionLaboral = exposicionLaboral;
	this.tratamientosRecibidos = tratamientosRecibidos;
	this.otrosTrabajos = otrosTrabajos;
}



function ExploracionFisicaDto(pesoReal, talla, imc, frecuenciaCardiaca,
		frecuenciaRespiratoria, temperatura, ta, craneo, cabello) {
	this.pesoReal = pesoReal;
	this.talla = talla;
	this.imc = imc;
	this.frecuenciaCardiaca = frecuenciaCardiaca;
	this.frecuenciaRespiratoria = frecuenciaRespiratoria;
	this.temperatura = temperatura;
	this.ta = ta;
	this.craneo = craneo;
	this.cabello = cabello;
}




function CaraDto(labiosBoca, organosDentales, lenguaFaringe, amigdalas,
		septumaNasal, cornetes, oidos, irisPupilas, conjuntivasEsclera,
		lineaVisual, utilizaLentes, agudezaVisual, agudezaVisual2,
		percepcionCromatica, visionCercana) {
	this.labiosBoca = labiosBoca;
	this.organosDentales = organosDentales;
	this.lenguaFaringe = lenguaFaringe;
	this.amigdalas = amigdalas;
	this.septumaNasal = septumaNasal;
	this.cornetes = cornetes;
	this.oidos = oidos;
	this.irisPupilas = irisPupilas;
	this.conjuntivasEsclera = conjuntivasEsclera;
	this.lineaVisual = lineaVisual;
	this.utilizaLentes = utilizaLentes;
	this.agudezaVisual = agudezaVisual;
	this.agudezaVisual2 = agudezaVisual2;
	this.percepcionCromatica = percepcionCromatica;
	this.visionCercana = visionCercana;
}


function RegionesSegmentosDto(cuello, areaPulmonar, areaCardiaca, abdomen,
		miembrosToracicos, miembrosPelvicos, columnaVertebral, genitalesExteriores) {
	this.cuello = cuello;
	this.areaPulmonar = areaPulmonar;
	this.areaCardiaca = areaCardiaca;
	this.abdomen = abdomen;
	this.miembrosToracicos = miembrosToracicos;
	this.miembrosPelvicos = miembrosPelvicos;
	this.columnaVertebral = columnaVertebral;
	this.genitalesExteriores = genitalesExteriores;
}

function ExpedientePacienteDto(pacienteDto, heredofamiliaresDto,
		noPatologicosDto, patologicosDto, laboralesDto,
		exploracionFisicaDto, caraDto, regionesSegmentosDto) {
	this.pacienteDto = pacienteDto;
	this.heredofamiliaresDto = heredofamiliaresDto;
	this.noPatologicosDto = noPatologicosDto;
	this.patologicosDto = patologicosDto;
	this.laboralesDto = laboralesDto;
	this.exploracionFisicaDto = exploracionFisicaDto;
	this.caraDto = caraDto;
	this.regionesSegmentosDto = regionesSegmentosDto;
}



function FiltroExpedienteDto(expediente, nombre, paterno, materno, user) {
	this.expediente = expediente;
	this.nombre = nombre;
	this.paterno = paterno;
	this.materno = materno;
	this.user = user;
}





