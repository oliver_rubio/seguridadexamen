var capturaPersona ;
 var player ; 
 const video ;
 const selectVideo ;
 const lblselectVideo ;
 var snapshotCanvas ;
 
 var startButton ;
 var captureButton ;
 var retakeButton ;
 var guardarButton ;
 var videoTracks;

 var handleSuccess = function(stream) {
   // Attach the video stream to the video element and
// autoplay.
   player.srcObject = stream;
   videoTracks = stream.getVideoTracks();
 };

 captureButton.addEventListener('click', function() {
	 var context = null;
	 if(capturaPersona == 1){
		context = snapshotCanvas.getContext('2d');
		context.drawImage(player, 0, 0, snapshotCanvas.width, snapshotCanvas.height);
		snapshotCanvas.style.display = "block";
	 }
 

	// context.drawImage(player, 0, 0, snapshotCanvas.width-30,
	// snapshotCanvas.height);
	// context.drawImage(player, 0, 0, 320, 240);
	// snapshotCanvas.style.display = "block";
	   player.style.display = "none";
	   captureButton.style.display = "none";
	   retakeButton.style.display = "block";
	   guardarButton.style.display = "block";
	   selectVideo.style.display = "none";
	   lblselectVideo.style.display = "none";
	
	   stopMediaTracks(currentStream);
 });

 function startVideo(capPer){
	 
	 player = document.getElementById('player'); 
	 video = document.getElementById('player');
	 selectVideo = document.getElementById('selectVideo');
	 lblselectVideo = document.getElementById('lblselectVideo');
	 snapshotCanvas = document.getElementById('snapshot');
	 
	 startButton = document.getElementById('start');
	 captureButton = document.getElementById('capture');
	 retakeButton = document.getElementById('retake');
	 guardarButton = document.getElementById('btnGuardar');
	 
	 
	 
	 
	 
	 
	 
	 capturaPersona = capPer;
   startButton.style.display = "none";
   if(capPer==1){
  	 snapshotCanvas.style.display = "none";
	}
	
   player.style.display = "block";
   captureButton.style.display = "block";
   retakeButton.style.display = "none";
   guardarButton.style.display = "none";
   selectVideo.style.display = "block";
   lblselectVideo.style.display = "block";
   cambiaCamara();
 }

     /* NUEVO CODIGO */
 
   let currentStream;

   function stopMediaTracks(stream) {
     stream.getTracks().forEach(track => {
       track.stop();
     });
   }

   function gotDevices(mediaDevices) {
     selectVideo.innerHTML = '';
// selectVideo.appendChild(document.createElement('option'));
 let count = 1;
 mediaDevices.forEach(mediaDevice => {
   if (mediaDevice.kind === 'videoinput') {
 const option = document.createElement('option');
         option.value = mediaDevice.deviceId;
         if(count == 1){
        	 option.selected = true;
             }
         const label = mediaDevice.label || `Camera ${count++}`;
         const textNode = document.createTextNode(label);
         option.appendChild(textNode);
         selectVideo.appendChild(option);
       }
     });
   }

   function cambiaCamara() {
     if (typeof currentStream !== 'undefined') {
   stopMediaTracks(currentStream);
 }
 const videoConstraints = {};
 if (selectVideo.value === '') {
   videoConstraints.facingMode = 'environment';
     } else {
       videoConstraints.deviceId = { exact: selectVideo.value };
     }
     const constraints = {
       video: videoConstraints,
       audio: false
     };
     navigator.mediaDevices
       .getUserMedia(constraints)
       .then(stream => {
         currentStream = stream;
         video.srcObject = stream;
         return navigator.mediaDevices.enumerateDevices();
       })
       .then(gotDevices)
       .catch(error => {
         console.error(error);
       });
   }
   

   navigator.mediaDevices.enumerateDevices().then(gotDevices);

   selectVideo.onchange = startVideo;