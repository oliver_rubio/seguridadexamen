/**
 * Utilerías para peticiones ajax
 * 
 * @returns
 */
function AjaxUtil() {
	
	/**
	 * Envía una petición con el método especificado, a la url especificada, con
	 * la información especificada y ejecuta los callbacks de éxito o error
	 * según corresponda;
	 */
	this.sendRequest = function (method, url, data, callbackSuccess, callbackError) {
	    $.ajax({
	        type: method,
	        contentType: "application/json",
	        url: url,
	        data: data,
	        dataType: 'json',
	        cache: false,
	        timeout: 2100000,
	        async: true,
	        beforeSend: function(data){
	        	$('#pleaseWaitDialog').modal();
	        },
	        success: function (data) {
	        	$('#pleaseWaitDialog').modal('hide');
	        	callbackSuccess(data);
	        },
	        error: function (e) {
	        	$('#pleaseWaitDialog').modal('hide');
	        	console.log(e);
	        	callbackError(e);
	        }
	    });
	}
	
	
	this.sendRequest2 = function (method, url, data, callbackSuccess, callbackError) {
	    $.ajax({
	        type: method,
	        contentType: "application/json",
	        url: url,
	        data: data,
	        dataType: 'json',
	        cache: false,
	        timeout: 2100000,
	        async: true,
	        beforeSend: function(data){
	        	$('#pleaseWaitDialog2').modal();
	        },
	        success: function (data) {
	        	$('#pleaseWaitDialog2').modal('hide');
	        	callbackSuccess(data);
	        },
	        error: function (e) {
	        	$('#pleaseWaitDialog2').modal('hide');
	        	console.log(e);
	        	callbackError(e);
	        }
	    });
	}
}
