

function JavascriptTemplate() {
	this.init = function() {
		setTimeout(function () {
			  $('#divSuccess').addClass('d-none');
		}, 7500);
    }
	
	/**
	 * Agrega una la fila
	 * <tr>...</tr>
	 * especificada en el parametro htmlRow antes de la fila del elemento en
	 * cuestión actualElement
	 */
	this.addRowBefore = function(actualRowElement, htmlRow, callBack) {
		htmlRow.insertBefore($(actualRowElement));
	}
	
	/**
	 * Valida que el campo sea numerico
	 */
	this.isNumeric = function(input) {
	    var number = /^\-{0,1}(?:[0-9]+){0,1}(?:\.[0-9]+){0,1}$/i;
	    var regex = RegExp(number);
	    return regex.test(input) && input.length>0;
	}
	/**
	 * Valida que el campo solo enteros
	 */
	this.isInteger = function(input) {
		if(Math.floor(input) == input && $.isNumeric(input)) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Agrega una la fila
	 * <tr>...</tr>
	 * especificada en el parametro htmlRow después de la fila del elemento en
	 * cuestión actualElement
	 */
	this.addRowAfter = function(actualRowElement, htmlRow, callBack) {
		htmlRow.insertAfter($(actualRowElement));
	}
	
	/**
	 * Redirige a la pantalla de error
	 */
	this.redirectError = function () {
		window.location.replace(_contextPath + 'error');
	}
	

	
	/**
	 * Verifica que el input
	 * no sea vacío o nulo
	 */
	this.isEmpty = function (input) {
		if (!factura.rfc.trim()) {
			return false;
		} else {
			return true;
		}
	}

}



