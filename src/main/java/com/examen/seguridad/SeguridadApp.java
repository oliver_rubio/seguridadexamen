package com.examen.seguridad;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeguridadApp {

	public static void main(String[] args) {
		SpringApplication.run(SeguridadApp.class, args);
	}
}
