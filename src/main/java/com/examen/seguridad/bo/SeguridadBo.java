package com.examen.seguridad.bo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.examen.seguridad.dao.ISeguridadDao;
import com.examen.seguridad.dto.SesionDto;

@Service("seguridadBo")
public class SeguridadBo {
	final static Logger logger = LogManager.getLogger(SeguridadBo.class);

	@Autowired
	private ISeguridadDao seguridadDao;

	public SesionDto getInfoSesion(String user) {
		return seguridadDao.getInfoSesion(user);
	}
}
