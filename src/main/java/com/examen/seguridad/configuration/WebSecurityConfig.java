package com.examen.seguridad.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{
	
	@Autowired
    DataSource dataSource;
	
	@Autowired
	Environment env;
	

	String[] resources = new String[]{
            "/include/**","/css/**","/icons/**","/img/**","/js/**","/layer/**", "/assets/**"
    };	
	
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
            .authorizeRequests()
	        .antMatchers(resources).permitAll()  
	        .antMatchers("/").permitAll()
	        .antMatchers(HttpMethod.POST).permitAll()
			.anyRequest().authenticated()
			.and()
            .formLogin()
                .loginPage("/login")
                .permitAll()
                .defaultSuccessUrl("/dashboard")
                .failureUrl("/login?error=true")
                .usernameParameter("username")
                .passwordParameter("password")
                .and()
            .logout().permitAll().and();
    }    

    
    @Autowired
    public  void configAuthentication(AuthenticationManagerBuilder auth)  {    	
        try {
			auth.jdbcAuthentication().dataSource(dataSource)
					.usersByUsernameQuery("SELECT slogin as username, spassword as password, true as enabled, dfechamodificacion as fechamodificacion " +
										  "FROM seguridad.c_user " +
										  "WHERE slogin = ?")
			        .authoritiesByUsernameQuery("SELECT slogin as username, sauthority as authority, dfechamodificacion as fechamodificacion " +
			        							"FROM seguridad.c_user csu INNER JOIN seguridad.e_seguridad_user_sec_role esusr on csu.cuser = esusr.cseguridaduser" +
								        		"						   INNER JOIN seguridad.c_seguridad_role csr on esusr.cseguridadrole = csr.cseguridadrole " +
								        		"WHERE slogin = ? " );
		} catch (Exception e) {
			e.printStackTrace();
		}
    }    
    
    @Bean
    public PasswordEncoder getPasswordEncoder() {
        PasswordEncoder encoder = new BCryptPasswordEncoder(4);
        return encoder;
    }
}
