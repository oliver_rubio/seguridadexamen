package com.examen.seguridad.dao;

import com.examen.seguridad.dto.SesionDto;

public interface ISeguridadDao {
	SesionDto getInfoSesion(String slogin);
}
