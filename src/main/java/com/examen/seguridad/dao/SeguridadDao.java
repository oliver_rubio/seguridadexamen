package com.examen.seguridad.dao;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.examen.seguridad.dto.SesionDto;

@Repository("seguridadSeguimientoDao")
public class SeguridadDao extends JdbcDaoSupport implements ISeguridadDao {

	@Autowired
	DataSource dataSource;
	
	@Autowired
	Environment env;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public SesionDto getInfoSesion(String slogin){
		logger.debug("ejecutando getInfoSesion [id=" + slogin + "]");
		SesionDto result;
		String query = "select * from seguridad.c_user where slogin = ?";
		result = this.getJdbcTemplate().queryForObject(query, new Object[] {slogin}, new SesionMapper());
		logger.debug("getInfoSesion ejecutado");
		return result;
	}
}
