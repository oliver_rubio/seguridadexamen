package com.examen.seguridad.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.examen.seguridad.dto.SesionDto;

public class SesionMapper implements RowMapper<SesionDto>{

	@Override
	public SesionDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		SesionDto dto = new SesionDto();
		dto.setCuser(rs.getInt("cuser"));
		dto.setSlogin(rs.getString("slogin"));
		dto.setSnombre(rs.getString("snombre"));
		dto.setSapellido_paterno(rs.getString("sapellido_paterno"));
		dto.setSapellido_materno(rs.getString("sapellido_materno"));
		dto.setDfechamodificacion(rs.getDate("dfechamodificacion"));
		return dto;
	}
}
