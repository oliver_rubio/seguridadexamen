package com.examen.seguridad.controller;

import java.sql.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import com.examen.seguridad.bo.SeguridadBo;
import com.examen.seguridad.dto.SesionDto;

@Controller
public class SeguridadController extends ControllerSupport{
	final static Logger logger = LogManager.getLogger(SeguridadController.class);
	
	@Autowired
	private SeguridadBo seguridadBo;
	
	@GetMapping(value = {"/login","/"})
	public String login(ModelMap model, HttpServletRequest request) {
		try {			
			return "pages/login";
		}catch (Exception e) {
			logger.error("Error inesperado", e);
			return "redirect:/error";
		}
	}
	
	@GetMapping(value = "/dashboard")
	public String inicio(ModelMap model, HttpServletRequest request) {
		try {			
			SesionDto sesionDto = seguridadBo.getInfoSesion(request.getRemoteUser());
			model.addAttribute("idUserr", request.getRemoteUser());
			request.getSession().setAttribute("snombreuser",sesionDto.getSnombre() + " " + sesionDto.getSapellido_paterno() + " " + sesionDto.getSapellido_materno());
			request.getSession().setAttribute("login", sesionDto.getSlogin());
			if (sesionDto.isPasswordExpired()) {
				return "pages/changepassword";
			} else {
				return "pages/dashboard";
			}
		}catch (Exception e) {
			logger.error("Error inesperado", e);
			return "redirect:/error";
		}
	}		
}
