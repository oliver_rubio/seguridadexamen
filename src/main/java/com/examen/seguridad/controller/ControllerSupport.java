package com.examen.seguridad.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

public class ControllerSupport {
	@Autowired
	private MessageSource messageSource;
	
	/**
	 * Devuelve el mensaje con los
	 * parámetros especificados
	 * @param message
	 * @param parameters
	 * @return
	 */
	protected String getMessage(String message, String [] parameters) {
	    Locale locale = new Locale("es_MX");
		return this.messageSource.getMessage(message, parameters, locale);
	}
	
	/**
	 * Devuelve el mensaje
	 * sin parámetros
	 * @param message
	 * @return
	 */
	protected String getMessage(String message) {
	    Locale locale = new Locale("es_MX");
		return this.messageSource.getMessage(message, null, locale);
	}
}
