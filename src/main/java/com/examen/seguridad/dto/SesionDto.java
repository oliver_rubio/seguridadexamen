package com.examen.seguridad.dto;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SesionDto {
	
	private static final long PASSWORD_EXPIRATION_TIME = 30L * 24L * 60L * 60L * 1000L;    // 30 days				
	
	private Integer cuser;
	private String slogin;
	private String snombre;
	private String sapellido_paterno;
	private String sapellido_materno;
	private Date dfechamodificacion;
	
    public boolean isPasswordExpired() {
        if (this.dfechamodificacion == null) return false;
        long currentTime = System.currentTimeMillis();
        long lastChangedTime = this.dfechamodificacion.getTime();         
        return currentTime > lastChangedTime + PASSWORD_EXPIRATION_TIME;
    }
}
