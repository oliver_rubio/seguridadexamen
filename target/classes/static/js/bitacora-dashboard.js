var _bitacoraDashboard;
var EXITOSO = 181;
var SIN_CORREO_ELECTRONICO = 183;
var PDF_NO_ENCONTRADO = 185;
var ERROR_GENERAL = 186;
var RESULTADO_NO_LIBERADO = 211;
var EXITOSO_RETRASADO = 212;

var ENTREGADO = 1;
var RECHAZADO = 2;
var RECHAZADO_TEMPORALMENTE = 3;
var INVALIDO = 4;

var COUNT_LIBERADOS = 0;
var COUNT_LIBERADOS_B = 0;
var COUNT_NO_LIBERADOS = 0;
var COUNT_NO_LIBERADOS_B = 0;
var COUNT_CON_CORREO = 0;
var COUNT_CON_CORREO_B = 0;
var COUNT_SIN_CORREO = 0;
var COUNT_SIN_CORREO_B = 0;
var COUNT_CON_ERROR = 0;
var COUNT_CON_ERROR_B = 0;
var COUNT_SIN_ERROR = 0;
var COUNT_SIN_ERROR_B = 0;
var COUNT_ENTREGADOS = 0;
var COUNT_ENTREGADOS_B = 0;
var COUNT_NO_ENTREGADOS = 0;
var COUNT_NO_ENTREGADOS_B = 0;
var COUNT_LEIDOS = 0;
var COUNT_LEIDOS_B = 0;
var COUNT_NO_LEIDOS = 0;
var COUNT_NO_LEIDOS_B = 0;

var selectedElem;
var selectedOrden;

$(document).ready(function() {
	_bitacoraDashboard = new Dashboard();
	_bitacoraDashboard.init();
});

function Dashboard() {

	/**
	 * Inicializa los componentes necesarios para operar
	 */
	this.init = function() {
		console.log("prueba");
		var jsonFullBitacora = $.parseJSON($('#idAttSeguimientoBitacora').val());

		var table = $('#tb_bitacora_diaria')
				.DataTable(
						{
							"data" : jsonFullBitacora,
							"language" : {
								"url" : "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
							},
							"columns" : [
									{
										"data" : "idOrden",
										"render" : function(data, type, full,
												meta) {
											return '<div style="display:inline; padding-left: 2px; padding-right: 2px;" role="group">'
													+ '<i data-toggle="collapse" idorden="'
													+ data
													+ '" data-target="#accordion-'
													+ data
													+ '" class="fas fa-plus-circle btn-examenes align-middle" style="font-size: 15px; color: #428BCA"></i>'
													+ '</div>&nbsp;&nbsp;<div class="align-middle" style="display:inline;">'
													+ data;
											+'</div>';
										}
									},
									{
										"data" : "cveOrden",
										"render" : function(data, type, full,
												meta) {
											if (full.idEstatus == RESULTADO_NO_LIBERADO
													|| full.idEstatus == ERROR_GENERAL
													|| full.idEstatus == SIN_CORREO_ELECTRONICO
													|| full.idEstatus == PDF_NO_ENCONTRADO) {
												return '<div class="text-center" style="font-weight: bold;">'
														+ data + '</div>'
											} else {
												return '<div class="text-center" style="font-weight: bold;">'
														+ '<a target="_blank" href="'
														+ full.linkResult
														+ '">'
														+ data
														+ '</a></div>'
											}
										}
									},
									{
										"data" : "nombrePaciente",
										"render" : function(data, type, full,
												meta) {
											return '<div class="text-left" style="padding-left: 10px;">'
													+ data + '</div>'
										}
									},
									{
										"data" : "correoPaciente",
										"render" : function(data, type, full,
												meta) {
											return '<div class="text-left">'
													+ data + '</div>'
										}
									},
									{
										"data" : "correoMedico",
										"render" : function(data, type, full,
												meta) {
											return '<div class="text-left">'
													+ data + '</div>'
										}
									},
									{
										"data" : "nombreCaptura",
										"render" : function(data, type, full,
												meta) {
											return '<div class="text-left" style="padding-left: 10px;">'
													+ data + '</div>'
										}
									},
									{
										"data" : "idEstatus",
										"render" : function(data, type, full,
												meta) {
											if (data == ERROR_GENERAL) {
												return '<div alt="alt-1" title="Error general." class="text-center"><i style="font-size: 18px;" class="fas fa-bug text-warning"></i></div>'
											}
											if (data == RESULTADO_NO_LIBERADO) {
												COUNT_NO_LIBERADOS++;
												return '<div alt="alt-0" class="text-center"><i style="font-size: 18px;" class="fas fa-times-circle text-danger"></i></div>'
											} else {
												COUNT_LIBERADOS++;
												return '<div alt="alt-2" class="text-center"><i style="font-size: 18px;" class="fas fa-check-square text-success"></i></div>'
											}
										}
									},
									{
										"data" : "idEstatus",
										"render" : function(data, type, full,
												meta) {
											if (data == ERROR_GENERAL) {
												return '<div alt="alt-2" title="Error general." class="text-center"><i style="font-size: 18px;" class="fas fa-bug text-warning"></i></div>'
											}
											if (data == RESULTADO_NO_LIBERADO) {
												return '<div alt="alt-1" class="text-center"><i style="font-size: 18px; color: #e6e6e6" class="fas fa-minus "></i></div>'
											}
											if (data == SIN_CORREO_ELECTRONICO) {
												COUNT_SIN_CORREO++;
												return '<div alt="alt-0" class="text-center"><i style="font-size: 18px;" class="fas fa-times-circle text-danger"></i></div>'
											} else {
												COUNT_CON_CORREO++;
												return '<div alt="alt-3" class="text-center"><i style="font-size: 18px;" class="fas fa-check-square text-success"></i></div>'
											}

										}
									},
									{
										"data" : "idEstatus",
										"render" : function(data, type, full,
												meta) {
											if (data == ERROR_GENERAL) {
												return '<div alt="alt-2" title="Error general." class="text-center"><i style="font-size: 18px;" class="fas fa-bug text-warning"></i></div>'
											}
											if (data == RESULTADO_NO_LIBERADO
													|| data == SIN_CORREO_ELECTRONICO) {
												return '<div alt="alt-1" class="text-center"><i style="font-size: 18px; color: #e6e6e6" class="fas fa-minus "></i></div>'
											}
											if (data == PDF_NO_ENCONTRADO) {
												COUNT_CON_ERROR++;
												return '<div alt="alt-0" title="No se ha encontrado el PDF con los resultados." class="text-center"><img src="'
														+ _contextPath
														+ 'img/icons/error-file.png"></img></div>'
											} else {
												COUNT_SIN_ERROR++;
												return '<div alt="alt-3" class="text-center"><i style="font-size: 18px;" class="fas fa-check-square text-success"></i></div>'
											}
										}
									},
									{
										"data" : "idEstatus",
										"render" : function(data, type, full,
												meta) {
											if (data == ERROR_GENERAL) {
												return '<div alt="alt-4" title="Error general." class="text-center"><i style="font-size: 18px;" class="fas fa-bug text-warning"></i></div>'
											}
											if (data == RESULTADO_NO_LIBERADO
													|| data == SIN_CORREO_ELECTRONICO
													|| data == PDF_NO_ENCONTRADO) {
												return '<div alt="alt-3" class="text-center"><i style="font-size: 18px; color: #e6e6e6" class="fas fa-minus "></i></div>'
											}
											if (full.entregado == 1) {
												COUNT_ENTREGADOS++;
												return '<div alt="alt-6" class="text-center"><i style="font-size: 18px;" class="fas fa-check-square text-success"></i></div>'
											}
											if (full.entregado == 2) {
												COUNT_NO_ENTREGADOS++;
												return '<div alt="alt-2" title="El servidor de correo del paciente rechazó la entrega." class="text-center"><i style="font-size: 18px;" class="fas fa-times-circle text-danger"></i></div>'
											}
											if (full.entregado == 3) {
												COUNT_NO_ENTREGADOS++;
												return '<div alt="alt-1" title="El servidor de correo del paciente rechazó temporalmente la entrega." class="text-center"><i style="font-size: 18px;" class="fas fa-times-circle text-danger"></i></div>'
											}
											if (full.entregado == 4) {
												COUNT_NO_ENTREGADOS++;
												return '<div alt="alt-0" title="El correo electrónico es inválido." class="text-center"><i style="font-size: 18px;" class="fas fa-times-circle text-danger"></i></div>'
											} else {
												COUNT_NO_ENTREGADOS++;
												return '<div alt="alt-5" title="Esperando entrega..." class="text-center"><i style="font-size: 18px; color: #e6e6e6" class="fas fa-ellipsis-h"></i></div>'
											}

										}
									},
									{
										"data" : "idEstatus",
										"render" : function(data, type, full,
												meta) {
											if (data == ERROR_GENERAL) {
												return '<div alt="alt-2" title="Error general." class="text-center"><i style="font-size: 18px;" class="fas fa-bug text-warning"></i></div>'
											}
											if (data == RESULTADO_NO_LIBERADO
													|| data == SIN_CORREO_ELECTRONICO
													|| data == PDF_NO_ENCONTRADO
													|| full.entregado != 1) {
												return '<div alt="alt-1" class="text-center"><i style="font-size: 18px; color: #e6e6e6" class="fas fa-minus "></i></div>'
											}
											if (!full.leido) {
												COUNT_NO_LEIDOS++;
												return '<div alt="alt-0" title="Esperando lectura..." class="text-center"><i style="font-size: 18px; color: #e6e6e6" class="fas fa-ellipsis-h"></i></div>'
											} else {
												COUNT_LEIDOS++;
												return '<div alt="alt-3" class="text-center"><i style="font-size: 18px;" class="fas fa-check-square text-success"></i></div>'
											}
										}
									} ],
							"columnDefs" : [ {
								type : 'alt-string',
								targets : [ 6, 7, 8, 9 ,10]
							} ],
							"order" : [ [ 0, "desc" ] ],
							scrollX : true,
						});

		COUNT_LIBERADOS_B = COUNT_LIBERADOS;
		COUNT_NO_LIBERADOS_B = COUNT_NO_LIBERADOS;
		COUNT_CON_CORREO_B = COUNT_CON_CORREO;
		COUNT_SIN_CORREO_B = COUNT_SIN_CORREO;
		COUNT_CON_ERROR_B = COUNT_CON_ERROR;
		COUNT_SIN_ERROR_B = COUNT_SIN_ERROR;
		COUNT_ENTREGADOS_B = COUNT_ENTREGADOS;
		COUNT_NO_ENTREGADOS_B = COUNT_NO_ENTREGADOS;
		COUNT_LEIDOS_B = COUNT_LEIDOS;
		COUNT_NO_LEIDOS_B = COUNT_NO_LEIDOS;

		$('#idLiberados').text(COUNT_LIBERADOS);
		$('#idConEmail').text(COUNT_CON_CORREO);
		$('#idSinError').text(COUNT_SIN_ERROR);
		$('#idEntregados').text(COUNT_ENTREGADOS);
		$('#idLeidos').text(COUNT_LEIDOS);

	}
	/*
	 * Evento: Click Muestra la gráfica
	 */
	$('body')
			.on(
					'click',
					'.card-estadistica',
					function() {
						data = _bitacoraDashboard.buildChart(this.id)
						var ctx = document.getElementById("chart-estadistica")
								.getContext('2d');
						var myDoughnutChart = new Chart(
								ctx,
								{
									type : 'doughnut',
									data : data,
									options : {
										tooltips : {
											callbacks : {
												label : function(tooltipItem,
														data) {
													var dataset = data.datasets[tooltipItem.datasetIndex];
													var total = dataset.data
															.reduce(function(
																	previousValue,
																	currentValue,
																	currentIndex,
																	array) {
																return previousValue
																		+ currentValue;
															});
													var currentValue = dataset.data[tooltipItem.index];
													var precentage = Math
															.floor(((currentValue / total) * 100) + 0.5);
													return precentage + "%";
												}
											}
										}
									}
								});

						$("#modalLiberados").modal("show")
					});

	/**
	 * Devuelve la información para trazar la gráfica
	 * 
	 */
	this.buildChart = function(id) {
		if (id == 'card-0') {
			$('#title-chart').text('Liberados');
			return {
				datasets : [ {
					data : [ COUNT_LIBERADOS_B, COUNT_NO_LIBERADOS_B ],
					backgroundColor : [ '#28A745', '#DC3545' ]
				} ],

				labels : [ 'Liberados: ' + COUNT_LIBERADOS_B,
						'No Liberados: ' + (COUNT_NO_LIBERADOS_B) ],

			};
		}
		if (id == 'card-1') {
			$('#title-chart').text('Con Email');
			return {
				datasets : [ {
					data : [ COUNT_CON_CORREO_B, COUNT_SIN_CORREO_B ],
					backgroundColor : [ '#28A745', '#DC3545' ]
				} ],

				labels : [ 'Email: ' + COUNT_CON_CORREO_B,
						'Sin Email: ' + COUNT_SIN_CORREO_B ],

			};
		}
		if (id == 'card-2') {
			$('#title-chart').text('Sin Error');
			return {
				datasets : [ {
					data : [ COUNT_SIN_ERROR_B, COUNT_CON_ERROR_B ],
					backgroundColor : [ '#28A745', '#DC3545' ]
				} ],

				labels : [ 'Sin Error: ' + COUNT_SIN_ERROR_B,
						'Con Error: ' + COUNT_CON_ERROR_B ],

			};
		}
		if (id == 'card-3') {
			$('#title-chart').text('Entregados');
			return {
				datasets : [ {
					data : [ COUNT_ENTREGADOS_B, COUNT_NO_ENTREGADOS_B ],
					backgroundColor : [ '#28A745', '#DC3545' ]
				} ],

				labels : [ 'Entregados: ' + COUNT_ENTREGADOS_B,
						'No Entregados: ' + COUNT_NO_ENTREGADOS_B ],

			};
		}
		if (id == 'card-4') {
			$('#title-chart').text('Leídos');
			return {
				datasets : [ {
					data : [ COUNT_LEIDOS_B, COUNT_NO_LEIDOS_B ],
					backgroundColor : [ '#28A745', '#DC3545' ]
				} ],

				labels : [ 'Leídos: ' + COUNT_LEIDOS_B,
						'No Leídos: ' + COUNT_NO_LEIDOS_B ],

			};
		}

	}

	/**
	 * Event: Click Realiza la búsqueda de los examenes realizados para la orden
	 * especificada
	 * 
	 * @returns
	 */
	$('body').on(
			'click',
			'.btn-examenes',
			function() {
				if (selectedOrden != $(this).attr('idorden')) {
					$('.collapse in').collapse()
					selectedOrden = $(this).attr('idorden');
					selectedElem = $(this).parent().parent().parent();
					var ajaxUtil = new AjaxUtil();
					ajaxUtil.sendRequest('get', _contextPath + 'examenes/'
							+ $(this).attr('idorden'), null,
							_bitacoraDashboard.showExamenes, null);
				} else {
					$('.collapse in').collapse()
					selectedOrden = "";
					selectedElem = "";
					setTimeout(function(){
						$(".dynamic-row").remove();
					}, 250);
					
				}
			});

	/**
	 * Muestra el modal con los examenes asociados a la orden
	 */
	this.showExamenes = function(callbackData) {
		$(".dynamic-row").remove();
		var dynamicRow = $('<tr class="dynamic-row border-bottom">');
		var dynamicCols = '<td colspan="10" style="padding-left: 25px; padding-right: 10px;"><div id="accordion-'
				+ selectedOrden + '" class="collapse">';
		dynamicCols += '<div class="row" style="font-weight: bold"><div class="col-sm-2"></div><div class="col-sm-1">Captura</div><div class="col-sm-1">Promesa</div><div class="col-sm-1">Tomador</div><div class="col-sm-1">Viaje</div>'
				+ '<div class="col-sm-1">Toma Muestra</div><div class="col-sm-1">Cierre</div><div class="col-sm-1">Recepción</div>'
				+ '<div class="col-sm-1">Entrega</div><div class="col-sm-1">Estatus</div></div>';

		$
				.each(
						callbackData,
						function(i, item) {
							var fixedNombreExamen
							if (item.nombreExamen.length > 25) {
								fixedNombreExamen = item.nombreExamen
										.substring(0, 25)
										+ '...';
							} else {
								fixedNombreExamen = item.nombreExamen;
							}
							var fixedNombreTomador
							if (item.nombreTomador.length > 15) {
								fixedNombreTomador = item.nombreTomador
										.substring(0, 15)
										+ '...';
							} else {
								fixedNombreTomador = item.nombreTomador;
							}
							var fixedFechaMuestra;
							if (item.fechaMuestra.length > 16) {
								fixedFechaMuestra = item.fechaMuestra
										.substring(0, 16)
										+ '...';
							} else {
								fixedFechaMuestra = item.fechaMuestra;
							}
							dynamicCols += '<div class="row" style="font-size: 11px; padding-top: 5px;"><div title="'
									+ item.nombreExamen
									+ '" class="col-sm-2" style="font-weight: bold; text-align: left !important;">'
									+ fixedNombreExamen
									+ '</div><div class="col-sm-1">'
									+ item.fechaCaptura
									+ '</div><div class="col-sm-1">'
									+ item.fechaPromesa
									+ '</div><div class="col-sm-1" title="'
									+ item.nombreTomador
									+ '">'
									+ fixedNombreTomador
									+ '</div><div class="col-sm-1">'
									+ item.viaje
									+ '</div>'
									+ '<div class="col-sm-1 text-left" title="' + item.fechaMuestra + '">'
									+ fixedFechaMuestra
									+ '</div><div class="col-sm-1 text-right">'
									+ item.fechaCierre
									+ '</div><div class="col-sm-1">'
									+ item.fechaRecepcion
									+ '</div>'
									+ '<div class="col-sm-1">'
									+ item.fechaEntregaResultado
									+ '</div><div class="col-sm-1 text-left">'
									+ item.estadoMuestra + '</div></div>';

						});
		dynamicCols += '</div></td>';
		dynamicRow.append(dynamicCols);
		dynamicRow.insertAfter($(selectedElem).closest("tr"));
		$('.collapse').collapse()

	}

	/**
	 * Elimina el estilo de la fila seleccionada al cerrar el modal
	 * 
	 * @returns
	 */
	$('#modalExamenes').on('hidden.bs.modal', function() {
		selectedElem.css("background-color", "")
	})
}