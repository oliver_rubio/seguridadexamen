/**
 * 
 */
var _login;
var _javascriptTemplate;


$(document).ready(function(){
	_javascriptTemplate = new JavascriptTemplate();
	_login = new Login();
	_login.init();
});

function mayus(e) {
    e.value = e.value.toUpperCase();
}


function Login(){
	
	this.init = function (){
		$('#modalSuccessLogin').modal('show');
		$('.select2-behavior').select2({
			theme : "bootstrap",
			width : "100%"
		});
		
		 $(".date-class").datepicker({
	            uiLibrary: 'bootstrap4',
	            format: 'dd/mm/yyyy'
	        });
				
		$( "#inputNombre" ).focus();	
	}
	
	
	$('.solo-numero').keyup(function (){
        this.value = (this.value + '').replace(/[^0-9]/g, '');
      });
  
  $('.input-number').on('input', function () { 
	    this.value = this.value.replace(/[^0-9||,]/g,'');
	});
  
	
  this.displayMessage = function (msg, type, time) {
	  	var title = 'Información:';	  
	  	if(type == 'success'){
	  		title = 'Proceso exitoso:';	
	  	}
	    
	    var type = 'success';
		var message = 'action was a danger!';
	    
	    var alertType = 'alert-'+ type
        
	       // alert('alert type is: '+ alertType);
	        
	        var htmlAlert = '<div class="alert '+ alertType +'"><h5>'+ type +'</h5><p>'+ message +'</p></div>';
	        
	       // alert(htmlAlert);
	        
	        $("#qz-alert").prepend(htmlAlert);
	        
	        $("#qz-alert .alert").first().hide().fadeIn(200).delay(time).fadeOut(1000, function () { $(this).remove(); });
	    
	    
	}
  
  
  
  $('body').on(
			'click',
			'#btoIngresar',
			function(){
				console.log("entra");
				
				_login.displayMessage('prueba', 'alert-danger',5000);				
			});
  
  $('body').on(
			'click',
			'#btoIniciaSesion',
			function(){
				_login.limpiarLogin();
				_login.limpiarRegistro();
				$('#formLogin').removeClass('d-none');
				$('#formRegister').addClass('d-none');			
				
				$('#divRegistrarse').removeClass('d-none');
				$('#divIniciaSesion').addClass('d-none');	
					
			});
    
  
  
  
  $('body').on(
			'click',
			'#btoRegistrarse',
			function(){
				_login.limpiarLogin();
				_login.limpiarRegistro();
				$('#formLogin').addClass('d-none');
				$('#formRegister').removeClass('d-none');			
				
				$('#divRegistrarse').addClass('d-none');
				$('#divIniciaSesion').removeClass('d-none');
			});
  
  
  
  
  
  this.limpiarLogin = function() {
	  $('#username').val("");
	  $('#password').val("");
	  
  }
  
  
  this.limpiarRegistro = function() {
	  $('#inputNombre').val("");
	  $('#inputPaterno').val("");
	  $('#inputMaterno').val("");
	  $('#inputCorreo').val("");
	  $('#inputTelefono').val("");
	  $('#inputUsername').val("");
	  $('#inputPassword').val("");
	  $('#confiPassword').val("");	  
  }
  
  
  
  
  $('body').on(
			'click',
			'#btoRegistrar',
			function(){
				
				
				var nombre = $('#inputNombre').val();
				var paterno = $('#inputPaterno').val();
				var materno = $('#inputMaterno').val();
				var sexo = 1;
				
				var feNacimiento = '01/01/2000';
				var direccion = '';
				var codigopostal = 0;
				
				var email = $.trim($('#inputCorreo').val());
				var telefono = '';
				var celular = $('#inputTelefono').val();
				
				var crol = 2;
				var ctipoexpediente = 1;
								
				var usuario = $('#inputUsername').val();
				var password = $('#inputPassword').val();
				
				
				altaDto = new AltaPersonaDto(null, nombre, paterno, materno, sexo, feNacimiento,
						direccion, codigopostal, email, telefono, celular, crol,
						ctipoexpediente, 0, 1, usuario, password);
				
					var jsonTecnico = JSON.stringify(altaDto);
					console.log(jsonTecnico);
					console.log(_contextPath);
//					if(_login.validate2(altaDto)){
						
						var ajax = new AjaxUtil();
						ajax.sendRequest('post',_contextPath+'empresa/persona/crear-persona',
								jsonTecnico,
								_login.manageSuccessAlta,
								_login.manageErrorAlta);
//					}
								
			});
	
	this.manageSuccessAlta = function(data) {	
		console.log(data);
		$('#msgDefaultSuccess').empty();
//		$('#msgDefaultSuccess').html('<h6>Nota de Crédito cancelada</h6>');
		$('#modalSuccess').modal('show');
		
	}
	
	this.manageErrorAlta = function(data) {
		console.log(data);
		if(data.responseText == null){
			$('#msgDefaultError').empty();
				$('#msgDefaultError').html('</br></br><h6>ERR: Se genero un error en la solicitud</h6></br>');
				$('#modalError').modal('show');
		}else{
			
			var response = $.parseJSON(data.responseText);
			$('#msgDefaultError').empty();
//		$('#msgDefaultError').html('</br></br><h6> Error al crear modalidad </h6></br>');
			$('#msgDefaultError').html('</br></br><h6>' +
					response.codigoError + ': '
					+ response.descripcionError + '</h6></br>');
			$('#modalError').modal('show');
		}
			
	}
  
}